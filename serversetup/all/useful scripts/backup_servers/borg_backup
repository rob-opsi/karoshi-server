#!/bin/bash

LogDate=$(date +%F)
ServerName=$(hostname-fqdn)
#Keep each servers borg config files in their own folder so that we can back them up more easily if we are using keyfile instead of repokey for the backup
export BORG_CONFIG_DIR="/root/.config/borg/$ServerName"

#Data input
ConfigFolderName="$1"
Action="$2"
ArchiveName="$3"
FolderPath="$4"

#Make log folder and check input
[ ! -d "/opt/karoshi/logs/backup_servers_offsite/$LogDate" ] && mkdir -p "/opt/karoshi/logs/backup_servers_offsite/$LogDate"

if [ -z "$ConfigFolderName" ]
then
	echo "$(date): $ServerName - "$"Blank configuration folder name." >> "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
	exit 1
fi

if [[ ! -f "/opt/karoshi/server_network/backup_servers_offsite/$ServerName/$ConfigFolderName/connect.cfg" ]]
then
	echo "$(date): $ServerName - $ConfigFolderName/connect.cfg - "$"The configuration file does not exist." >> "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
	exit 1
fi

if [ ! -z "$Action" ]
then
	if [[ "$Action" != TestSSHConnection ]] && [[ "$Action" != viewarchives ]] && [[ "$Action" != viewarchivefiles ]] && [[ "$Action" != restorefiles ]]
	then
		echo "$(date): $ServerName - invalid action." >> "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
		exit 1
	fi
else
	Action=DoBackup
fi

if [ "$Action" = viewarchives ]
then
	if [ -z "$ArchiveName" ]
	then
		echo "$(date): $ServerName - blank archivename." >> "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
		exit 1
	fi	
fi

#Check if the backup is disabled
if [ -f "/opt/karoshi/server_network/backup_servers_offsite/$ServerName/$ConfigFolderName/stop_backup" ]
then
	#echo "$(date): $ServerName - "$"This backup is disabled." | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
	exit 1
fi

#Make sure borgbackup is installed on the server
if [[ ! $(which borg) ]]
then
	echo "$(date): $ServerName - "$"Installing Borg Backup." | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
	if [[ $(lsb_release -a 2>/dev/null | grep -c 16.04) -gt 0 ]]
	then
		add-apt-repository -y ppa:costamagnagianfranco/borgbackup
	fi
	apt-get update
	apt-get -y install borgbackup
fi

#Make sure jq is installed on the server
if [[ ! $(which jq) ]]
then
	echo "$(date): $ServerName - "$"Installing jq." | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
	apt-get -y install jq
fi

source "/opt/karoshi/server_network/backup_servers_offsite/$ServerName/$ConfigFolderName/connect.cfg"

#Make sure we are not already doing a backup for this backup configuration.
if [ "$Action" = DoBackup ]
then
	BackupCount=$(ps aux | grep -c "borg_backup $BACKUPUSERNAME.$BACKUPSERVERNAME")
	if [[ "$BackupCount" -gt 3 ]]
	then
		echo "$(date): $ServerName - "$"A backup was already running for"" $BACKUPUSERNAME.$BACKUPSERVERNAME." | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
		exit 101
	fi
fi

[ -z "$ENCMODE" ] && ENCMODE="repokey-blake2"

function check_borg_installed_remote_server {
if [[ ! $(which borg) ]]
then
	#No Borg!
	exit 101
else
	#We have Borg!
	exit 100
fi
}

function make_archive_folders {

for ArchiveName in $(echo "$ArchiveNames" | sed 's/,/ /g' )
do
	if [ ! -d "$STORAGEPATH/$ServerName/$ArchiveName" ]
	then
		echo $"Creating: ""$STORAGEPATH/$ServerName/$ArchiveName"
		mkdir -p "$STORAGEPATH/$ServerName/$ArchiveName"
	fi
done
}


function TestSSH {
#Check if we are using ssh keys or a password
if [ -z "$BACKUPPASSWORD" ]
then
	SSHAccess=keys
else
	SSHAccess=password
fi
#Check that we can connect to server and check if borg is installed
if [ "$SSHAccess" = keys ]
then
	echo "$(date): $BACKUPSERVERNAME - "$"Using ssh keys to check the ssh connection to this server." >> "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
	ssh -o PasswordAuthentication=no -o ConnectTimeout=10 "$BACKUPUSERNAME@$BACKUPSERVERNAME" "$(declare -f check_borg_installed_remote_server);
	check_borg_installed_remote_server"
else
	echo "$(date): $BACKUPSERVERNAME - "$"Using the supplied password to check the ssh connection to this server." >> "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
	sshpass -p "$BACKUPPASSWORD" ssh "$BACKUPUSERNAME@$BACKUPSERVERNAME" "$(declare -f check_borg_installed_remote_server);
	check_borg_installed_remote_server"
fi
SSHStatus="$?"
if [ "$SSHStatus" = 101 ]
then
	RemoteBorgInstalled=no
	echo "$(date): $BACKUPSERVERNAME - "$"The SSH connection is working. "$"Borg is not installed on this backup server."" "$"SSHFS will be used to mount the backup folder on the remote server which will be slower." >> "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
elif [ "$SSHStatus" = 100 ]
then
	RemoteBorgInstalled=yes
	echo "$(date): $BACKUPSERVERNAME - "$"The SSH connection is working. "$"Borg is installed on this backup server." >> "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
else
	echo "$(date): $BACKUPSERVERNAME - "$"Cannot connect via ssh to this backup server." >> "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
	exit "$SSHStatus"
fi

if [ "$Action" = TestSSHConnection ]
then
	exit "$SSHStatus"
fi
}

function MountSSHBackupDrive {
#Needed if borg is not installed on the remote ssh server
[ ! -d "/mnt/offsite-backup-sshfs/$ArchiveName" ] && mkdir -p "/mnt/offsite-backup-sshfs/$ArchiveName"
if [ "$SSHAccess" = keys ] 
then
	ArchiveNameMnt=$(echo "$ArchiveName" | cut -d: -f1)
	sshfs "$BACKUPUSERNAME@$BACKUPSERVERNAME:/$STORAGEPATH/$ServerName/$ArchiveNameMnt" "/mnt/offsite-backup-sshfs/$ArchiveNameMnt" 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
elif [ "$SSHAccess" = password ]
then
	sshpass -p "$BACKUPPASSWORD" sshfs "$BACKUPUSERNAME@$BACKUPSERVERNAME:/$STORAGEPATH/$ServerName/$ArchiveName" "/mnt/offsite-backup-sshfs/$ArchiveName" 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
fi
}

function UnMountSSHBackupDrive {
umount "/mnt/offsite-backup-sshfs/$ArchiveNameMnt" 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
}

function MountLocalBackupDrive {
#Unmount the drive
if [[ $(mount | grep -c -w /mnt/"$LABEL") -gt 0 ]]
then
	umount /mnt/"$LABEL"/
fi

#Mount the drive
[ ! -d /mnt/"$LABEL"/ ] && mkdir -p /mnt/"$LABEL"/
mount -L "$LABEL" /mnt/"$LABEL"/
if [ "$?" != 0 ]
then
	echo "$(date): $ServerName - $LABEL - "$"The backup disk with this label could not be mounted." | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
	exit
fi

STORAGEPATH=$(echo /mnt/"$LABEL"/"$STORAGEPATH" | sed 's%//%/%g')
}

function UnMountLocalBackupDrive {
#Unmount the backup drive
umount /mnt/"$LABEL"/
if [ "$?" != 0 ]
then
	echo "$(date): $ServerName - $LABEL "$"The backup disk could not be unmounted." | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
fi
}

function DoBackup {

local ArchiveNames=$(cut -d, -f2 "/opt/karoshi/server_network/backup_servers_offsite/$ServerName/$ConfigFolderName/backup.cfg")
ArchiveNames=$(echo $ArchiveNames | sed 's/ /,/g')

if [ "$BACKUPTYPE" = ssh ]
then
	if [ "$SSHAccess" = keys ]
	then
		ssh -o PasswordAuthentication=no -o ConnectTimeout=10 "$BACKUPUSERNAME@$BACKUPSERVERNAME" "$(declare -f make_archive_folders);
		STORAGEPATH=$STORAGEPATH
		ServerName=$ServerName
		ArchiveNames=$ArchiveNames
		make_archive_folders"
	else
		sshpass -p "$BACKUPPASSWORD" ssh "$BACKUPUSERNAME@$BACKUPSERVERNAME" "$(declare -f make_archive_folders);
		STORAGEPATH=$STORAGEPATH
		ServerName=$ServerName
		ArchiveNames=$ArchiveNames
		make_archive_folders"
	fi
fi


if [ "$BACKUPTYPE" = local ]
then
	make_archive_folders
fi


for BACKUPDATA in $(cat "/opt/karoshi/server_network/backup_servers_offsite/$ServerName/$ConfigFolderName/backup.cfg")
do
	local BackupFolder=$(echo "$BACKUPDATA" | cut -d, -f1)
	local ArchiveName=$(echo "$BACKUPDATA" | cut -d, -f2)
	local HourlyArchives=$(echo "$BACKUPDATA" | cut -d, -f3)
	local DailyArchives=$(echo "$BACKUPDATA" | cut -d, -f4)
	local WeeklyArchives=$(echo "$BACKUPDATA" | cut -d, -f5)
	local MonthlyArchives=$(echo "$BACKUPDATA" | cut -d, -f6)
	local BackupSnapShotName=$(date "+%Y-%m-%d %H:%M")

	#Check if the folder to be backed up actually exists
	if [ ! -d "$BackupFolder" ]
	then
		echo "$(date): $ServerName - $BackupFolder does not exist." | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
		continue
	fi

	#Borg will not follow symlinks to lets make sure we have the right path.
	BackupFolder=$(readlink -f "$BackupFolder")

	echo "$(date): $ServerName - initialising borg archive for $ArchiveName using encryption mode $ENCMODE on $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"

	if [ "$BackupFolder" = /var/lib/mysql_backup ]
	then
		#Backup any mysql / mariadb databases to /var/lib/mysql_backup so that they can be backed up to the backup server
		/opt/karoshi/serversetup/all/"useful scripts"/backup_servers/mysql_backup
	fi

	if [ "$BACKUPTYPE" = ssh ]
	then
		if [ "$SSHAccess" = keys ] && [ "$RemoteBorgInstalled" = yes ]
		then
			#Init
			borg init --encryption="$ENCMODE" "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName" 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			#Do the backup
			echo "$(date): $ServerName - backing up $ArchiveName to $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			borg create --stats --compression auto,zlib,4 "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName::$BackupSnapShotName" "$BackupFolder" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			#Prune
			echo "$(date): $ServerName - pruning $ArchiveName on $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			borg prune -v --list --keep-hourly="$HourlyArchives" --keep-daily="$DailyArchives" --keep-weekly="$WeeklyArchives" --keep-monthly="$MonthlyArchives" "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName" 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"

		elif [ "$SSHAccess" = password ] && [ "$RemoteBorgInstalled" = yes ]
		then
			#Init
			sshpass -p "$BACKUPPASSWORD" borg init --encryption="$ENCMODE" "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName" 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			#Do the backup
			echo "$(date): $ServerName - backing up $ArchiveName to $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			sshpass -p "$BACKUPPASSWORD" borg create --stats --compression auto,zlib,4 "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName::$BackupSnapShotName" "$BackupFolder" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			#Prune
			echo "$(date): $ServerName - pruning $ArchiveName on $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			sshpass -p "$BACKUPPASSWORD" borg prune -v --list --keep-hourly="$HourlyArchives" --keep-daily="$DailyArchives" --keep-weekly="$WeeklyArchives" --keep-monthly="$MonthlyArchives" "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName" 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
		fi

		if [ "$RemoteBorgInstalled" = no ]
		then
			MountSSHBackupDrive
			if [ "$?" = 0 ]
			then
				#Init
				borg init --encryption="$ENCMODE" "/mnt/offsite-backup-sshfs/$ArchiveNameMnt" 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
				echo "$(date): $ServerName - backing up $ArchiveName to $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
				#Do the backup
				borg create --stats --compression auto,zlib,4 "/mnt/offsite-backup-sshfs/$ArchiveNameMnt::$BackupSnapShotName" "$BackupFolder" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
				#Prune
				echo "$(date): $ServerName - pruning $ArchiveName on $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
				borg prune -v --list --keep-hourly="$HourlyArchives" --keep-daily="$DailyArchives" --keep-weekly="$WeeklyArchives" --keep-monthly="$MonthlyArchives" "/mnt/offsite-backup-sshfs/$ArchiveNameMnt" 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			fi
			UnMountSSHBackupDrive
		fi

	fi

	if [ "$BACKUPTYPE" = local ]
	then
		#Init
		borg init --encryption="$ENCMODE" "$STORAGEPATH/$ServerName/$ArchiveName" 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
		echo "$(date): $ServerName - backing up $ArchiveName to $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
		#Do the backup
		borg create --stats --compression auto,zlib,4 "$STORAGEPATH/$ServerName/$ArchiveName::$BackupSnapShotName" "$BackupFolder" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
		#Prune
		echo "$(date): $ServerName - pruning $ArchiveName on $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
		borg prune -v --list --keep-hourly="$HourlyArchives" --keep-daily="$DailyArchives" --keep-weekly="$WeeklyArchives" --keep-monthly="$MonthlyArchives" "$STORAGEPATH/$ServerName/$ArchiveName" 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
	fi
	echo "$(date): $ServerName - backup completed for $ArchiveName on $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
done
echo "$(date): $ServerName - all backups completed on $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
}


#Test the ssh connection
if [ "$BACKUPTYPE" = ssh ] || [ "$Action" = TestSSHConnection ]
then
	TestSSH
fi

#Mount local backup drive
if [ "$Action" = DoBackup ] && [ "$BACKUPTYPE" = local ]
then
	MountLocalBackupDrive
fi

export BORG_PASSPHRASE="$PASSPHRASE"

#Create backup folders
if [ "$Action" = DoBackup ]
then
	DoBackup
fi

#Unmount local backup drive
if [ "$Action" = DoBackup ] && [ "$BACKUPTYPE" = local ]
then
	UnMountLocalBackupDrive
fi

if [ "$Action" = viewarchives ]
then
	#Show a list of all of the archives for the selected backup folder
	if [ "$BACKUPTYPE" = ssh ]
	then
		if [ "$RemoteBorgInstalled" = yes ]
		then
			if [ "$SSHAccess" = keys ]
			then
				ArchiveResults=$(borg list --short "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName" | sed 's/ /,/g' | sort -r 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log")
			else
				ArchiveResults=$(sshpass -p "$BACKUPPASSWORD" borg list --short "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName" | sed 's/ /,/g' | sort -r 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log")
			fi
		fi
		if [ "$RemoteBorgInstalled" = no ]
		then
			MountSSHBackupDrive
			if [ "$?" = 0 ]
			then
				ArchiveResults=$(borg list --short "/mnt/offsite-backup-sshfs/$ArchiveNameMnt" | sed 's/ /,/g' | sort -r 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log")
			fi
			UnMountSSHBackupDrive
		fi
	fi

	if [ "$BACKUPTYPE" = local ]
	then
		MountLocalBackupDrive
		ArchiveResults=$(borg list --short "$STORAGEPATH/$ServerName/$ArchiveName" | sed 's/ /,/g' | sort -r 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log")
		UnMountLocalBackupDrive
	fi

	for ArchiveData in $(echo "$ArchiveResults")
	do
		ArchiveName=$(echo "$ArchiveData" | cut -d, -f1)
		ArchiveDate=$(echo "$ArchiveData" | cut -d, -f2)	
		echo "Archive,$ArchiveName,$ArchiveDate"
	done
fi

if [ "$Action" = viewarchivefiles ]
then
	[ -z "$FolderPath" ] && FolderPath=$(echo "$ArchiveName" | cut -d: -f1)
	if [ "$BACKUPTYPE" = ssh ]
	then
		if [ "$RemoteBorgInstalled" = yes ]
		then
			if [ "$SSHAccess" = keys ]
			then
				borg list --json-lines "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName" "$FolderPath/" --exclude "$FolderPath/*/" | jq -r '.mode + "," + .user + "," + .group + "," + .path' 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			else
				sshpass -p "$BACKUPPASSWORD" borg list --json-lines "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName" "$FolderPath/" --exclude "$FolderPath/*/" | jq -r '.mode + "," + .user + "," + .group + "," + .path' 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			fi
		fi

		if [ "$RemoteBorgInstalled" = no ]
		then
			MountSSHBackupDrive
			if [ "$?" = 0 ]
			then
				borg list --json-lines "/mnt/offsite-backup-sshfs/$ArchiveName" "$FolderPath/" --exclude "$FolderPath/*/" | jq -r '.mode + "," + .user + "," + .group + "," + .path' 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			fi
			UnMountSSHBackupDrive
		fi

	fi

	if [ "$BACKUPTYPE" = local ]
	then	
		MountLocalBackupDrive
		borg list --json-lines "$STORAGEPATH/$ServerName/$ArchiveName" "$FolderPath/" --exclude "$FolderPath/*/" | jq -r '.mode + "," + .user + "," + .group + "," + .path' 2>>"/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
		UnMountLocalBackupDrive	
	fi
fi

if [ "$Action" = restorefiles ]
then
	cd /
	echo "$(date): $ServerName - Restoring $ArchiveName "$FolderPath" from $BACKUPSERVERNAME" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"

	if [ "$BACKUPTYPE" = ssh ]
	then
		if [ "$RemoteBorgInstalled" = yes ]
		then
			if [ "$SSHAccess" = keys ]
			then
				borg extract --list "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName" "$FolderPath" 2>&1 | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"				
			else
				sshpass -p "$BACKUPPASSWORD" borg extract --list "$BACKUPUSERNAME@$BACKUPSERVERNAME:$STORAGEPATH/$ServerName/$ArchiveName" "$FolderPath" 2>&1 | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			fi	
		fi

		if [ "$RemoteBorgInstalled" = no ]
		then
			MountSSHBackupDrive
			if [ "$?" = 0 ]
			then
				borg extract --list "/mnt/offsite-backup-sshfs/$ArchiveName" "$FolderPath" 2>&1 | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
			fi
			UnMountSSHBackupDrive
		fi
	fi

	if [ "$BACKUPTYPE" = local ]
	then	
		MountLocalBackupDrive
		borg extract --list "$STORAGEPATH/$ServerName/$ArchiveName" "$FolderPath" 2>&1 | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
		UnMountLocalBackupDrive	
	fi
	echo "$(date): $ServerName - Restore $ArchiveName "$FolderPath" to $BACKUPSERVERNAME completed" | tee -a "/opt/karoshi/logs/backup_servers_offsite/$LogDate/$ConfigFolderName.log"
fi

#Unset the passphrase
export BORG_PASSPHRASE=""
exit

