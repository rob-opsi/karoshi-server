#!/bin/bash
#Copyright (C) 2012  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"View E-Mail Statistics"
TITLEHELP=$"Choose the date that you want to view the email statistics for."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php"


if [ ! -z /opt/karoshi/server_network/emailserver ]
then
	EMAILSERVER=$(sed -n 1,1p /opt/karoshi/server_network/emailserver)
	TITLE="$TITLE - $EMAILSERVER"
fi

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser
source /opt/karoshi/web_controls/version

############################
#Language
############################

STYLESHEET=defaultstyle.css
TIMEOUT=300
NOTIMEOUT=127.0.0.1
#Get current date and time
DAY=$(date -d "-1 day" +"%d")
MONTH=$(date -d "-1 day" +"%m")
YEAR=$(date -d "-1 day" +"%Y")

HOUR=$(date +%H)
MINUTES=$(date +%M)
SECONDS=$(date +%S)

#########################
#Get data input
#########################

DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')

function show_status {
echo '
<script>
	alert("'$MESSAGE'");
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

echo '
<form action="/cgi-bin/admin/email_statistics.cgi" name="testform" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Log date"'</td>
				<td>'
echo "
					<!-- calendar attaches to existing form element -->
				<input class=\"karoshi-input\" type=\"text\" value=\"$DAY-$MONTH-$YEAR\" size=14 maxlength=10 name=\"_DATE_\">
				</td>
				<td style=\"vertical-align: top; text-align: center;\">
					<script>
					new tcal ({
						// form name
						'formname': 'testform',
						// input name
						'controlname': '_DATE_'
					});

					</script>"

echo '
				</td>
			</tr>
			<tr>
				<td>'$"View logs by date"'</td>
				<td></td>
				<td style="vertical-align: top; text-align: center;"><input id="Today" checked="checked" name="_LOGVIEW_" value="today" type="radio"><label for="Today"> </label></td>
			</tr>
			<tr>
				<td>'$"View logs by month"'</td>
				<td></td>
				<td style="vertical-align: top; text-align: center;"><input id="Month" name="_LOGVIEW_" value="month" type="radio"><label for="Month"> </label></td>
			</tr>
		</tbody>
	</table>'

#Show list of email servers
/opt/karoshi/web_controls/show_servers "$MOBILE" email $"Email Statistics"

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
