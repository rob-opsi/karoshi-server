#!/bin/bash
#Copyright (C) 2007  The karoshi Team

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Language
TITLE=$"Edit a Remote Management User"
ERRORMSG1="The user action must not be blank."
ERRORMSG2="You cannot delete yourself."
HTTPS_ERROR="You must access this page via https."
ACCESS_ERROR1="You must be a Karoshi Management User to complete this action."

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=5
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign USERACTION
DATANAME=USERACTION
get_data
USERACTION="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/remote_management_view.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
function view_users {
echo '
<script>
	window.location = "/cgi-bin/admin/remote_management_view.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE="$HTTPS_ERROR"
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$ACCESS_ERROR1
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE="$ACCESS_ERROR1"
	show_status
fi
#########################
#Check data
#########################
#Check to see that USERACTION is not blank
if [ -z "$USERACTION" ]
then
	MESSAGE=$ERRORMSG1
	show_status
fi
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/remote_management_process.cgi | cut -d' ' -f1)
if [[ $(echo "$USERACTION" | grep -c deleteuser) = 1 ]]
then
	USERNAME="${USERACTION//deleteuser/}"
	if [ "$USERNAME" != "$REMOTE_USER" ]
	then
		sudo -H /opt/karoshi/web_controls/exec/remote_management_delete "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$USERNAME"
		view_users
	else
		MESSAGE="$ERRORMSG2"
		show_status
	fi
fi

if [[ $(echo "$USERACTION" | grep -c edituser) = 1 ]]
then
	echo '
	<form action="/cgi-bin/admin/remote_management_view.cgi" method="post">
		<button class="button" formaction="remote_management_view.cgi" name="_ViewUsers_" value="_">
			'$"View"'
		</button>
	</form>

<form action="/cgi-bin/admin/remote_management_edit.cgi" method="post">'
	USERNAME="${USERACTION//edituser/}"
	sudo -H /opt/karoshi/web_controls/exec/remote_management_edit2 "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$USERNAME"
	echo '<input value="Submit" class="button primary" type="submit"> <input value="Reset" class="button" type="reset"></display-karoshicontent></body></html>'
fi
exit
