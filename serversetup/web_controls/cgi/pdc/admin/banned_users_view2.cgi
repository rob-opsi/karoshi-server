#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Ban User Account"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
DATA_ARRAY=( `echo $DATA | sed 's/_USERNAME_/_USERNAME_ /g' | sed 's/_VIEWLOG_/_VIEWLOG_ /g'` )
END_POINT=`echo ${#DATA_ARRAY[@]}`
let END_POINT=$END_POINT*2
#Assign USERARRAY
COUNTER=2
ARRAY_COUNT=0
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = USERNAMEcheck ]
	then
		let COUNTER=$COUNTER+1
		USERARRAY[$ARRAY_COUNT]=`echo $DATA | cut -s -d'_' -f$COUNTER`
		let ARRAY_COUNT=$ARRAY_COUNT+1
	fi
	let COUNTER=$COUNTER+1
done

#Assign VIEWARRAY
COUNTER=2
ARRAY_COUNT=0
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = VIEWLOGcheck ]
	then
		let COUNTER=$COUNTER+1
		VIEWARRAY[$ARRAY_COUNT]=`echo $DATA | cut -s -d'_' -f$COUNTER`
		let ARRAY_COUNT=$ARRAY_COUNT+1
	fi
	let COUNTER=$COUNTER+1
done

#########################
#Assign data to variables
#########################
END_POINT=4

function show_status {
echo '
<script>
alert("'$MESSAGE'");
	window.location = "/cgi-bin/admin/banned_users_view_fm.cgi";
</script>
</display-karoshicontent>
</body></html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/banned_users_view2.cgi | cut -d' ' -f1)
if [ ! -z "$USERARRAY" ]
then
	#Allow users
	sudo -H /opt/karoshi/web_controls/exec/banned_users_view2 "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$(echo ${USERARRAY[@]:0} | sed 's/ /:/g')"
	#View any chosen logs
	MESSAGE=$"The selected users have been removed from the banned list."
	show_status
fi

if [ ! -z "$VIEWARRAY" ]
then
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:`echo ${VIEWARRAY[@]:0} | sed 's/ /:/g'`" | sudo -H /opt/karoshi/web_controls/exec/incident_log_view2
fi
echo "</display-karoshicontent></body></html>"
