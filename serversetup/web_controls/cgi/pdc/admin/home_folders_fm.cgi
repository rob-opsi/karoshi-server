#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Home Folders"
TITLEHELP=$"This displays the server that hosts the home folders for each group."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Home_Folders"

ICON3=/images/submenus/system/computermed.png

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form action="/cgi-bin/admin/home_folders.cgi" method="post">'

#Check for gluster support
if [[ $(grep -c dfs /etc/samba/smb.conf) -gt 0 ]]
then
	echo '
<button class="button" formaction="gluster_control.cgi" name="GlusterVolumeControl" value="_">
	'$"Gluster Volumes"'
</button>
	'
fi

echo '
<button class="button" formaction="samba_shares.cgi" name="SambaShares" value="_">
	'$"Network Shares"'
</button>
</form>
<form action="/cgi-bin/admin/home_folders.cgi" method="post">
<table id="myTable" class="tablesorter">
	<thead>
		<tr>
			<th class="karoshi-input"><b>'$"Primary Group"'</b></th>
			<th class="karoshi-input"><b>'$"Server"'</b></th>
			<th style="width: 100px;"><b>'$"Change"'</b></th>
		</tr>
	</thead>
	<tbody>
'



for PRI_GROUP in /opt/karoshi/server_network/group_information/*
do
	PRI_GROUP=$(basename "$PRI_GROUP")
	unset GLUSTERVOL
	source /opt/karoshi/server_network/group_information/"$PRI_GROUP"

	#Check for gluster volume
	[ -z "$GLUSTERVOL" ] && GLUSTERVOL=notset
	if [ -d /opt/karoshi/server_network/gluster-volumes/"$GLUSTERVOL" ]
	then
		ICON3=/images/submenus/system/glustermed.png
	else
		ICON3=/images/submenus/system/computermed.png
	fi


		echo '
		<tr>
			<td>'"$PRI_GROUP"'</td>
			<td>'"${SERVER//,/<br>}"'</td>
			<td>
				<button class="info" name="_ChangeServer_" value="_PRIGROUP_'"$PRI_GROUP"'_SERVER_'"$SERVER"'_">
					<img src="'"$ICON3"'" alt="'$"Rename"'">
					<span>'$"Change Server"'<br><br>'"$PRI_GROUP"'<br><br>'"${SERVER//,/<br>}"'</span>
				</button>
			</td>
		</tr>
		'
done

echo '
	</tbody>
</table>
</form>
</display-karoshicontent>
</body>
</html>'
exit


########################
#Random Key
#+hi8yv5gR9WNhAlMONd2e3VYQo9ErIHPoMcd4auuw8g=
########################
