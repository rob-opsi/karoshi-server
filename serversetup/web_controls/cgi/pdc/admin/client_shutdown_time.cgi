#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Client Shutdown time"
TITLEHELP=$"This sets the time that the client computers should turn off at the end of the day."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Client_Shutdown_Time"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
			<display-karoshicontent>
			<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/client_shutdown_time.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER": /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

echo '
		<form action="/cgi-bin/admin/client_shutdown_time2.cgi" method="post">'

#Get shutdown time
if [ -f /var/lib/samba/netlogon/domain_information/clientshutdowntime ]
then
	SHUTDOWNTIME=$(sed -n 1,1p /var/lib/samba/netlogon/domain_information/clientshutdowntime | tr -cd '0-9:')
	HOUR=$(echo "$SHUTDOWNTIME" | cut -d: -f1)
	MINUTES=$(echo "$SHUTDOWNTIME" | cut -d: -f2)
fi

if [ -f /var/lib/samba/netlogon/domain_information/idletime ]
then
	IDLETIME=$(sed -n 1,1p /var/lib/samba/netlogon/domain_information/idletime)
fi

echo '
				<table>
					<tbody>
						<tr>
							<td class="karoshi-input">'$"Shutdown time"'</td>
							<td><input style="width: 60px;" maxlength="2" size="2" value="'"$HOUR"'" name="_HOUR_" type="text">:<input style="width: 60px;" maxlength="2" size="2" value="'"$MINUTES"'" name="_MINUTES_" type="text"></td>
							<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Client_Shutdown_Time"><span class="icon-large-tooltip">'$"Enter the time that you want the clients to shutdown."'<br><br>'$"Leave this blank to disable the client shutdown."'</span></a></td>
						</tr>
						<tr>
							<td>'$"Idle time"'</td>
							<td><input style="width: 60px;" maxlength="2" size="2" value="'"$IDLETIME"'" name="_IDLETIME_" type="text"></td>
							<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Client_Shutdown_Time"><span class="icon-large-tooltip">'$"Time to wait before shutting down if not in use."'<br><br>'$"Leave this blank to disable idle shutdown."'</span></a>
							</td>
						</tr>
					</tbody>
				</table>
				<input value="Submit" class="button primary" type="submit"></div>
			</form>
		</display-karoshicontent>
	</body>
</html>'
exit
