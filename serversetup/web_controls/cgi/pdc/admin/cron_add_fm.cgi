#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Schedule Job"
TITLEHELP=$"Choose the servers you want to view the scheduled commands on."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=View_Scheduled_Jobs"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
		<form action="/cgi-bin/admin/cron_add.cgi" method="post">

			<button class="button" formaction="/cgi-bin/admin/cron_view_fm.cgi" name="SelectServer" value="_">
				'$"Scheduled Jobs"'
			</button>
			<br><br>
			<table>
	    			<tbody>
	      				<tr>
						<td class="karoshi-input">'$"Minutes"'</td>
						<td><select name="___MINUTES___" class="karoshi-input">
							<option value="0">0</option>
							<option value="5">5</option>
							<option value="10">10</option>
							<option value="15">15</option>
							<option value="20">20</option>
							<option value="25">25</option>
							<option value="30">30</option>
							<option value="35">35</option>
							<option value="40">40</option>
							<option value="45">45</option>
							<option value="50">50</option>
							<option value="55">55</option>
							<option value="*">'$"Every minute"'</option>
							<option value="*/5">'$"Every"' 5 '$"Minutes"'</option>
							<option value="*/10">'$"Every"' 10 '$"Minutes"'</option>
							<option value="*/15">'$"Every"' 15 '$"Minutes"'</option>
							<option value="*/20">'$"Every"' 20 '$"Minutes"'</option>
							<option value="*/30">'$"Every"' 30 '$"Minutes"'</option>
							</select></td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=View_Scheduled_Jobs"><span class="icon-large-tooltip">'$"Choose the minute you want the job to start on."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Hour"'</td>
						<td><select name="___HOUR___" class="karoshi-input">
							<option value="*">'$"Every hour"'</option>
							<option>0</option>
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							<option>6</option>
							<option>7</option>
							<option>8</option>
							<option>9</option>
							<option>10</option>
							<option>11</option>
							<option>12</option>
							<option>13</option>
							<option>14</option>
							<option>15</option>
							<option>16</option>
							<option>17</option>
							<option>18</option>
							<option>19</option>
							<option>20</option>
							<option>21</option>
							<option>22</option>
							<option>23</option>
							</select></td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=View_Scheduled_Jobs"><span class="icon-large-tooltip">'$"Choose the hour you want the job to start on."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Day"'</td>
						<td><select name="___DAY___" class="karoshi-input">
							<option value="*">'$"Every day"'</option>
							<option>0</option>
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							<option>6</option>
							<option>7</option>
							<option>8</option>
							<option>9</option>
							<option>10</option>
							<option>11</option>
							<option>12</option>
							<option>13</option>
							<option>14</option>
							<option>15</option>
							<option>16</option>
							<option>17</option>
							<option>18</option>
							<option>19</option>
							<option>20</option>
							<option>21</option>
							<option>22</option>
							<option>24</option>
							<option>25</option>
							<option>26</option>
							<option>27</option>
							<option>28</option>
							<option>29</option>
							<option>30</option>
							<option>31</option>
							</select></td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=View_Scheduled_Jobs"><span class="icon-large-tooltip">'$"Choose the day of the month you want the job to start on."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Month"'</td>
						<td><select name="___MONTH___" class="karoshi-input">
							<option value="*">'$"Every month"'</option>
							<option>0</option>
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							<option>6</option>
							<option>7</option>
							<option>8</option>
							<option>9</option>
							<option>10</option>
							<option>11</option>
							<option>12</option>
							</select></td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=View_Scheduled_Jobs"><span class="icon-large-tooltip">'$"Choose the month you want the job to start on."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Day of week"'</td>
						<td><select name="___DOFW___" class="karoshi-input">
							<option value="1-7">'$"Every day"'</option>
							<option value="1-5">'$"Every week day"'</option>
							<option value="6-7">'$"Weekend"'</option>
							<option value="1-3-5">'$"Monday"'-'$"Wednesday"'-'$"Friday"'</option>
							<option value="1">'$"Monday"'</option>
							<option value="2">'$"Tuesday"'</option>
							<option value="3">'$"Wednesday"'</option>
							<option value="4">'$"Thursday"'</option>
							<option value="5">'$"Friday"'</option>
							<option value="6">'$"Saturday"'</option>
							<option value="7">'$"Sunday"'</option>
							</select></td>
						<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=View_Scheduled_Jobs"><span class="icon-large-tooltip">'$"Choose the days of the week you want the job to run on."'</span></a></td>
					</tr>
	      				<tr>
						<td>'$"Command"'</td>
						<td><input tabindex= "2" name="___COMMAND___" size="25" style="width: '"$WIDTH2"'px;" type="text"></td>
						<td><a class="info" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the command or path to the command that you want to run."'</span></a></td>
					</tr>
				</tbody>
			</table>
			<br><br>'

#Show list of servers
/opt/karoshi/web_controls/show_servers "$MOBILE" servers $"Schedule Job" notset notset ___


echo '</form></display-karoshicontent></body></html>'

exit
