#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Add Windows Server"
TITLEHELP=$"This will add the details required to remotely run commands on a windows server."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
			<form action="/cgi-bin/admin/windows_servers_add.cgi" method="post">
			<table>
				<tbody>
					<tr>
						<td class="karoshi-input">'$"Server name"'</td>
						<td><input class="karoshi-input" tabindex= "1" name="_SERVERNAME_" size="23" type="text"></td><td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the name of the windows server you want to add."'</span></a></td>
					</tr>
					<tr>
						<td>'$"TCPIP number"'</td><td><input class="karoshi-input" tabindex= "2" maxlength="15" name="_TCPIPNUMBER_" size="23" type="text"></td><td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the tcpip number of the server you want to add."'</span></a></td></tr>
					<tr>
						<td>'$"Administrator user"'</td>
						<td><input class="karoshi-input" tabindex= "3" name="_ADMINUSER_" size="23" type="text"></td><td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the administrator password. If you change the password on the server you will need to change it here as well."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Administrator password"'</td>
						<td><input class="karoshi-input" tabindex= "4" name="_PASSWORD1_" size="23" type="password"></td><td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the administrator password. If you change the password on the server you will need to change it here as well."'</span></a></td>
					</tr>
					<tr>
						<td>'$"Confirm"'</td>
						<td><input class="karoshi-input" tabindex= "5" name="_PASSWORD2_" size="23" type="password"></td>
					</tr>
					<tr>
						<td>'$"Role"'</td>
						<td><textarea  tabindex= "6" cols="50" rows="4" name="_ROLE_"></textarea></td><td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in a description of the role of the server."'</span></a></td>
					</tr>
				</tbody>
			</table>
			<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">

			</form>
		</display-karoshicontent>
	</body>
</html>'
exit

