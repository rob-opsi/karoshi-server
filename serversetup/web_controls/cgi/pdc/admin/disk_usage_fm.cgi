#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Disk Usage"
TITLEHELP=$"Choose the server that you want to view the disk usage on."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Disk_Usage"

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form id="foo" action="/cgi-bin/admin/disk_usage.cgi" name="selectservers" method="post">'

#Redirect to show the disk information if there is only one server
if [ $(ls -1 /opt/karoshi/server_network/servers/ | wc -l) = 1 ]
then
	echo '
	<input name="_SERVERNAME_'`hostname-fqdn`'_SERVERTYPE_network_SERVERMASTER_notset_MOBILE_'$MOBILE'_" value="" type="hidden">
	<script type="text/javascript">
	    function myfunc () {
		var frm = document.getElementById("foo");
		frm.submit();
	    }
	    window.onload = myfunc;
	</script>'
else
	#Show list of servers
	/opt/karoshi/web_controls/show_servers $MOBILE servers $"Show disk usage"
fi

echo '</form></display-karoshicontent></body></html>'

exit
