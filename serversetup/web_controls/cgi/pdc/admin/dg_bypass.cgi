#!/bin/bash
#Copyright (C) 2014  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Client Bypass Controls"
TITLEHELP=$"This can be used for client devices to bypass the internet filtering."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Bypass_Controls"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	0: { sorter: "ipAddress" }
    		}
		});
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=7
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign TCPIP
DATANAME=TCPIP
get_data
TCPIP="$DATAENTRY"

[ -z "$ACTION" ] && ACTION=view

#Generate navigation bar
if [ "$MOBILE" = no ]
then
	WIDTH=100
	ICON1=/images/submenus/system/add.png
	ICON2=/images/submenus/system/computer.png
	#Generate navigation bar
	/opt/karoshi/web_controls/generate_navbar_admin
else
	WIDTH=90
	ICON1=/images/submenus/system/addm.png
	ICON2=/images/submenus/system/computerm.png
fi

function passinfo {
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/dg_bypass.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$TCPIP:$MOBILE:" | sudo -H /opt/karoshi/web_controls/exec/dg_bypass
}

BUTTONACTION=add
BUTTONMSG=$"Add IP number"
ICON="$ICON1"
if [ $ACTION = add ]
then
	BUTTONACTION=view
	BUTTONMSG=$"View IP numbers"
	ICON="$ICON2"
	
fi


echo '
	<form action="/cgi-bin/admin/dg_bypass.cgi" name="selectedsites" method="post">
		<button class="button" name="_Add_" value="_ACTION_'"$BUTTONACTION"'_">
			'"$BUTTONMSG"'
		</button>
	</form>'

if [ "$ACTION" = reallyadd ]
then
	passinfo
fi

if [ "$ACTION" = view ] || [ "$ACTION" = delete ]
then
	passinfo
fi

if [ "$ACTION" = add ]
then 
	echo '<form action="/cgi-bin/admin/dg_bypass.cgi" name="selectedsites" method="post"><input type="hidden" name="_ACTION_" value="reallyadd">'
	if [ "$MOBILE" = yes ]
	then
		echo '
	'$"TCPIP Number"'<br>
	<input class="karoshi-input" name="_TCPIP_" value="'"$TCPIP_ADDR"'" size="20"><br><br>
	'
	else
		echo '
		<table class="standard" style="text-align: left;" >
	    		<tbody>
				<tr>
					<td class="karoshi-input">'$"TCPIP Number"'</td>
					<td><input required="required" name="_TCPIP_" value="'"$TCPIP_ADDR"'" class="karoshi-input" size="20"></td>
					<td><a class="info" href="javascript:void(0)"><img class="images" alt="" src="/images/help/info.png"><span>'$"Enter in the tcpip number of the client computer that you want to set to bypass the filtering."'</span></a></td>
				</tr>
			</tbody>
		</table>'
	fi
	echo '<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset"></form>'
fi

echo '</display-karoshicontent></body></html>'
exit
