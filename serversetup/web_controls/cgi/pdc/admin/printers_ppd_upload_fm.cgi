#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Upload PPD file"
TITLEHELP=$"PPD can be downloaded from the openprinting.org website."
HELPURL="http://openprinting.org/printer_list.cgi"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/printers.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#Get printer details
if [ ! -f /var/www/karoshi/uploadppd ]
then
	MESSAGE=$"No Printer details found."
	show_status
fi
source /var/www/karoshi/uploadppd
source /opt/karoshi/web_controls/version

ICON1=/images/submenus/printer/view_print_queues.png
ICON2=/images/submenus/printer/delete_printer.png
ICON3=/images/submenus/file/folder.png

echo '
<FORM ENCTYPE="multipart/form-data" ACTION="/cgi-bin/admin/printers_ppd_upload.cgi" METHOD="POST">

	<button formaction="/cgi-bin/admin/printers.cgi" class="button" name="_ShowPrinters_" value="_">
		'$"Show Printers"'
	</button>

	<button formaction="/cgi-bin/admin/printers_delete.cgi" class="button" name="_DeletePrinters_" value="_">
		'$"Delete Printer"'
	</button>

	<button formaction="/cgi-bin/admin/locations.cgi" class="button" name="_ViewLocations_" value="_">
		'$"Locations"'
	</button>

</form>
<FORM ENCTYPE="multipart/form-data" ACTION="/cgi-bin/admin/printers_ppd_upload.cgi" METHOD="POST">

	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Printer"'</td>
				<td>'"$PRINTERNAME"'</td>
				<td></td>
			</tr>
			<tr>
				<td>'$"Default Page Size"'</td>
				<td>'"$PAGESIZE"'</td>
				<td></td>
			</tr>
			<tr>
				<td>'$"Print in Colour?"'</td>
				<td>'"$COLOUR"'</td>
				<td></td>
			</tr>
			<tr>
				<td>'$"Printer PPD"'</td>
				<td><INPUT TYPE="FILE" NAME="file-to-upload-01" SIZE="25"></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"If you are using a Linux client with cups installed you can find ppd files in /usr/share/cups/models or /usr/share/ppd."'</span></a></td>
			</tr>
		</tbody>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>
'
exit
