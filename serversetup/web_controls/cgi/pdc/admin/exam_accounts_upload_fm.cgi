#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Exam Accounts - Upload Files"
TITLEHELP=$"The uploaded files will be copied to a folder called exam_files in each exam account."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Exam_Accounts#Copy_Data_to_Exam_Accounts"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
	<form action="/cgi-bin/admin/exam_accounts_view_reset_passwords.cgi" method="post">
		<button formaction="/cgi-bin/admin/exam_accounts.cgi" class="button">
			'$"Manage Exam Accounts"'
		</button>
	</form>

	<FORM ENCTYPE="multipart/form-data" ACTION="/cgi-bin/admin/exam_accounts_upload.cgi" METHOD="POST">
	'$"Select the files that you want to upload for the exam accounts"':<br>

        <table>
        <tr>
            <td>
                '$"File"' 1:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-01" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 2:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-02" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 3:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-03" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 4:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-04" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 5:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-05" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 6:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-06" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 7:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-07" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 8:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-08" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 9:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-09" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 10:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-10" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 11:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-11" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 12:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-12" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 13:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-13" SIZE="35">
            </td>
        </tr>
        <tr>
            <td>
                '$"File"' 14:
            </td>
            <td>
                <INPUT TYPE="FILE" NAME="file-to-upload-14" SIZE="35">
            </td>
        </tr>
        </table>
  	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
        </form>
</display-karoshicontent>
</body>
</html>
'
exit
