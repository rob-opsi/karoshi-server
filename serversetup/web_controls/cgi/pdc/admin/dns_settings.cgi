#!/bin/bash
#Copyright (C) 2016  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+*')
#CONVERT STAR
DATA=$(echo "$DATA" | sed 's/*/%99/g')
#echo $DATA"<br>"
#########################
#Assign data to variables
#########################
END_POINT=25

function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

#Assign SERVERTYPE
DATANAME=SERVERTYPE
get_data
SERVERTYPE="$DATAENTRY"

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

END_POINT=45

#Assign DNS1
DATANAME=DNS1
get_data
DNS1="$DATAENTRY"

#Assign DNS2
DATANAME=DNS2
get_data
DNS2="$DATAENTRY"

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"DNS Settings"' '"$SERVER2"
TITLEHELP=$"View and edit the DNS settings for your servers."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=DNS_Settings"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/dns_settings.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#Check data
if [ -z "$ACTION" ]
then
	ACTION=view
fi

if [ "$ACTION" = reallyedit ]
then
	if [ -z "$SERVERNAME" ]
	then
		MESSAGE=$"The servername cannot be blank."
		show_status
	fi

	if [ -z "$DNS1" ]
	then
		MESSAGE=$"You have not entered in a DNS entry."
		show_status
	fi
	#Check that the tcpip number is valid.

	#Check dots and max number.
	if [[ $(echo "$DNS1" | sed 's/\./\n /g'  | sed /^$/d | wc -l) != 4 ]] || [[ $(echo "$DNS1" | sed 's/\./\n /g'  | sed /^$/d | sort -g -r | sed -n 1,1p) -gt 255 ]]
	then
		MESSAGE=$"You have entered in an incorrect DNS entry."
		show_status
	fi
	if [ ! -z "$DNS2" ]
	then
		if [[ $(echo "$DNS2" | sed 's/\./\n /g'  | sed /^$/d | wc -l) != 4 ]] || [[ $(echo "$DNS1" | sed 's/\./\n /g'  | sed /^$/d | sort -g -r | sed -n 1,1p) -gt 255 ]]
		then
			MESSAGE=$"You have entered in an incorrect DNS entry."
			show_status
		fi
	fi
fi

if [ -z "$SERVERNAME" ]
then
	SERVERNAME=notset
fi
if [ -z "$SERVERTYPE" ]
then
	SERVERTYPE=notset
fi

if [ "$SERVERTYPE" = federatedslave ]
then
	#Assign SERVERMASTER
	DATANAME=SERVERMASTER
	get_data
	SERVERMASTER="$DATAENTRY"
fi


if [ "$SERVERNAME" != notset ]
then
	echo '
		<form action="/cgi-bin/admin/dns_settings.cgi" method="post">
			<button class="button" name="_ChooseServer_" value="_">
				'$"Choose Server"'
			</button>
		</form>
	'
else
	echo '
		<form action="/cgi-bin/admin/dnsview.cgi" method="post">
			<button class="button" name="_ViewDNSEntries_" value="_">
				'$"View DNS Entries"'
			</button>
		</form>
	'
fi

echo '<form action="/cgi-bin/admin/dns_settings.cgi" method="post">'

if [ "$ACTION" = reallyedit ] || [ "$ACTION" = autogenerate ]
then
	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/dns_settings.cgi | cut -d' ' -f1)
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$SERVERNAME:$SERVERTYPE:$SERVERMASTER:$ACTION:$DNS1:$DNS2:" | sudo -H /opt/karoshi/web_controls/exec/dns_set_forwarder	
	ACTION=view
fi

if [ "$ACTION" = edit ]
then
	#Get DNS settings for the server
	DNSLIST=( $(sudo -H /opt/karoshi/web_controls/exec/get_dns_forwarders "$SERVERNAME") )
	DNSLISTCOUNT=${#DNSLIST[*]} 

	FORMACTION=reallyedit
	[ "$DNSLISTCOUNT" -gt 1 ] && FORMACTION=autogenerate

	#Show form with the current dns servers on it


	echo '<input type="hidden" name="_SERVERNAME_'"$SERVERNAME"'_">
	<input type="hidden" name="_SERVERTYPE_'"$SERVERTYPE"'_">
	<input type="hidden" name="_SERVERMASTER_" value="'"$SERVERMASTER"'">
	<input type="hidden" name="_ACTION_" value="'$FORMACTION'">
	<table><tbody>
	<tr><td class="karoshi-input">'$"Servername"'</td><td class="karoshi-input">'"$SERVERNAME"'</td></tr>'

	COUNTER=0
	COUNTER1=1
	while [ "$COUNTER" -lt "$DNSLISTCOUNT" ]
	do
		echo '<tr><td>'$"DNS Server"' 1</td><td>'
		if [ "$FORMACTION" = reallyedit ]
		then
			echo '<input required="required" tabindex= "'"$COUNTER1"'" class="karoshi-input" name="_DNS'"$COUNTER1"'_" value="'"${DNSLIST[$COUNTER]}"'"  type="text">'
		else
			echo "${DNSLIST[$COUNTER]}"
		fi
	echo '</td></tr>'
		let COUNTER="$COUNTER"+1
		let COUNTER1="$COUNTER1"+1		
	done
	echo '</tbody></table>'
	[ "$FORMACTION" = autogenerate ] && echo ''$"This server uses your Domain Controllers for DNS."' '$"Press submit to auto regenerate the DNS settings."'<br><br>'
	echo '<input value="'$"Submit"'" class="button primary" type="submit">'
	[ "$FORMACTION" = reallyedit ] && echo ' <input value="'$"Reset"'" class="button" type="reset">'
fi

#Show list of servers
if [ "$ACTION" = view ]
then
	/opt/karoshi/web_controls/show_servers "$MOBILE" servers $"Set DNS Forwarder" "edit" "showdns"
fi

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
