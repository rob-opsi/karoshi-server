#!/bin/bash
#modify_groups
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Bulk User Actions"
TITLEHELP=$"This will affect a group of users."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Bulk_User_Actions"

MOD_CODE="${RANDOM:0:3}"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Check password settings
source /opt/karoshi/server_network/security/password_settings

#Custom scripts
echo '
<script>
function showHide(type){
	if(type == "changeprigroup"){
		document.getElementById("NewPrimaryGroup1").style.display = "block";
		document.getElementById("NewPrimaryGroup2").style.display = "block";
	} else {
		document.getElementById("NewPrimaryGroup1").style.display = "none";
		document.getElementById("NewPrimaryGroup2").style.display = "none";
	}
	if(type == "changepasswords"){
		document.getElementById("ChangePasswords1").style.display = "block";
		document.getElementById("ChangePasswords2").style.display = "block";
		document.getElementById("ChangePasswords3").style.display = "block";
	} else {
		document.getElementById("ChangePasswords1").style.display = "none";
		document.getElementById("ChangePasswords2").style.display = "none";
		document.getElementById("ChangePasswords3").style.display = "none";
	}

}
</script>'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form action="/cgi-bin/admin/modify_groups.cgi" method="post">

<button class="button" formaction="bulk_user_creation_upload_fm.cgi" name="_BulkUserCreation_" value="_">
	'$"Bulk User Creation"'
</button>

<button class="button" formaction="user_image_upload_fm.cgi" name="_ImportUserImages_" value="_">
	'$"Import User Images"'
</button>

<button class="button" formaction="bulk_user_creation_import_enrollment_numbers_fm.cgi" name="_ImportEnrollmentNumbers_" value="_">
	'$"Import Enrollment Numbers"'
</button>

<button class="button" formaction="csv_set_passwords_fm.cgi" name="_SetUserPasswords_" value="_">
	'$"Set User Passwords"'
</button>
</form>
<form action="/cgi-bin/admin/modify_groups.cgi" method="post">

<table>
	<tbody>
	<tr>
		<td class="karoshi-input">'$"Primary Group"'</td>
		<td>'
/opt/karoshi/web_controls/group_dropdown_list | sed 's/_GROUP_/____GROUP____/g'
echo '		</td>
		<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Bulk_User_Actions"><span class="icon-large-tooltip">'$"All users in the group you select will be affected by the action you choose from this menu."'</span></a></td>
	</tr>
	<tr>
		<td>'$"Option"'</td>
		<td>
			<select onchange="showHide(this.value)" name="____OPTIONCHOICE____" class="karoshi-input">
				<option value="enable">'$"Enable"'</option>
				<option value="disable">'$"Disable"'</option>
				<option value="deleteaccounts">'$"Delete accounts"'</option>
				<option value="deleteaccounts2">'$"Archive and delete accounts"'</option>
				<option value="resetpasswords">'$"Reset passwords"'</option>
				<option value="changepasswords">'$"Change passwords"'</option>
				<option value="changepassnextlogon">'$"Change password on next login"'</option>
				<option value="passwordsneverexpire">'$"Passwords never expire"'</option>
				<option value="passwordsexpire">'$"Passwords expire"'</option>
				<option value="changeprigroup">'$"Change Primary Group "'</option>
			</select></td>
		<td></td>
	</tr>
	<tr>
		<td><div id="ChangePasswords1" style="display: none;">New Password</div></td>
		<td>
			<input id="ChangePasswords2" style="display: none;" pattern=".{'"$MINPASSLENGTH"',128}" title="'$"Password length required:"' '"$MINPASSLENGTH"'" tabindex= "2" class="karoshi-input" name="____NEWPASSWORD____" size="20" type="password">
		</td>
		<td><a id="ChangePasswords3" style="display: none;" class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Change_Password"><span class="icon-large-tooltip">'$"Enter in the new password that you want the user to have."'<br><br>'$"Leave the password fields blank if you want a random password."'<br><br>'$"The following special characters are allowed"'<br><br> space !	&quot;	# 	$	%	&amp; 	(	) 	*	+	, 	-	.	/ 	:
;	&lt;	=	&gt;	?	@ 	[	\	]	^	_	` 	{	|	}	~<br><br>'

[ "$PASSWORDCOMPLEXITY" = on ] && echo ''$"Upper and lower case characters and numbers are required."'<br><br>'
echo ''$"The Minimum password length is "''"$MINPASSLENGTH"'.<br></span></a></td>
	</tr>
	<tr>
		<td><div id="NewPrimaryGroup1" style="display: none;">New Primary Group</div></td>
		<td>'
/opt/karoshi/web_controls/group_dropdown_list | sed 's/_GROUP_/____NEWGROUP____/g' | sed 's/class="karoshi-input"/class="karoshi-input" style="display: none;" id="NewPrimaryGroup2"/g'
echo '
		</td>
		<td></td>
	</tr>
	<tr>
		<td>'$"Exceptions"'</td>
		<td><input tabindex= "1" name="____EXCEPTIONLIST____" class="karoshi-input" size="20" type="text"></td>
		<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Bulk_User_Actions"><span class="icon-large-tooltip">'$"Enter in any user accounts that you do not want to modify separated by spaces."'</span></a></td>
	</tr>
	<tr>
		<td>'$"Modify Code"'</td><td><b>'"$MOD_CODE"'</b></td>
		<td></td>
	</tr>
	<tr>
		<td>'$"Confirm"'</td>
		<td><input required="required" name="____MODCODE____" class="karoshi-input" size="3" type="number" min="0" max="999"></td>
		<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Modify_Groups"><span class="icon-large-tooltip">'$"Enter in the code displayed on the page to confirm this action."'</span></a></td>
	</tr>
</tbody>
</table>
<input name="____FORMCODE____" value="'"$MOD_CODE"'" type="hidden">
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form></display-karoshicontent></body></html>'
exit

