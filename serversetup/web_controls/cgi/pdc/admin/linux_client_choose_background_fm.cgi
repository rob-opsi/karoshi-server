#!/bin/bash
#Copyright (C) 2012  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Linux Client Background"
TITLEHELP=$"Choose the background that you want for your linux clients."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Linux_Client_Background"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

echo '
<form action="/cgi-bin/admin/linux_client_background_upload_fm.cgi" method="post">
	<button class="button" name="_UPLOAD_" value="_">
		'$"Upload Background"'
	</button>
</form>
'

function upload_background {
echo '<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/linux_client_background_upload_fm.cgi";
</script>
</body>
</display-karoshicontent>
</html>'
exit
}

#Check to see if any backgrounds have been uploaded
if [ ! -d /var/lib/samba/netlogon/linuxclient/backgrounds ]
then
	MESSAGE=$"No client backgrounds have been uploaded."
	upload_background
fi

if [[ $(ls -1 /var/lib/samba/netlogon/linuxclient/backgrounds | wc -l) = 0 ]]
then
	MESSAGE=$"No client backgrounds have been uploaded."
	upload_background
fi

echo '
<form action="/cgi-bin/admin/linux_client_choose_background.cgi" method="post">
	<table id="myTable" class="tablesorter">
		<thead>
			<tr>
				<th class="karoshi-input">'$"Background Name"'</th>
				<th class="karoshi-input">'$"Preview"'</th>
				<th style="text-align:center">'$"Status"'</th>
				<th style="text-align:center">'$"Delete"'</th>			
			</tr>
		<tbody>'

#Get the default background
DEFAULTBACKGROUND=notset
[ -f /var/lib/samba/netlogon/linuxclient/backgrounds/defaultbackground ] && source /var/lib/samba/netlogon/linuxclient/backgrounds/defaultbackground


function show_background_info {
	echo '
			<tr>
				<td class="karoshi-input">'"$BACKGROUND_SHORT"'</td>
				<td><img class="karoshi-input" src="/images/linuxclient/backgrounds/'"$BACKGROUND"'.png" alt="'"$BACKGROUND"'"></td>
				<td style="text-align:center">'
	if [ "$BACKGROUND" = "$DEFAULTBACKGROUND" ]
	then
		echo '
					'$"Default Background"'
				</td><td>'

	else
		echo '
					<button class="button" name="___ChooseBackground___" value="___ACTION___choose___BACKGROUND___'"$BACKGROUND"'___">
						'$"Set Background"'	
					</button>
				</td><td>
					<button class="info" name="___DeleteBackground___" value="___ACTION___delete___BACKGROUND___'"$BACKGROUND"'___">
						<img src="/images/submenus/file/delete.png" alt="'$"Delete"'<br>'"$BACKGROUND"'">
					<span>'$"Delete"'<br>'"$BACKGROUND"'</span>
					</button>
'
	fi

	echo '
				</td>
			</tr>'
}

function get_background_info {
for BACKGROUNDS in /var/lib/samba/netlogon/linuxclient/backgrounds/*.png
do
	BACKGROUND=$(basename "$BACKGROUNDS" | sed 's/.png$//g')
	BACKGROUND_SHORT="${BACKGROUND:0:16}"

	if [ "$ShowDefault" = yes ]
	then
		if [ "$BACKGROUND" = "$DEFAULTBACKGROUND" ]
		then
			show_background_info
		fi
	else
		if [ "$BACKGROUND" != "$DEFAULTBACKGROUND" ]
		then
			show_background_info
		fi		

	fi
done
}

#Find default background
ShowDefault=yes
get_background_info
ShowDefault=no
get_background_info


echo '
		</tbody>
	</table>
</form>
</display-karoshicontent>
</body>
</html>'
exit

