#!/bin/bash
#Copyright (C) 2015  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser
source /opt/karoshi/web_controls/version

TITLE=$"Backup Essential Data"
TITLEHELP=$"Backup essential data for restoring the server in the evident of a re-install."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Backup_Essential_Data"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form action="/cgi-bin/admin/backup_essentials.cgi" name="selectservers" method="post">

'$"This will create an archive of configuration files that you can download and store on a memory stick for later use if you have to re-install the server."'
<br><br>
<b>'$"Note:"'</b> '$"This is only a backup of the server essential configuration data."'<br><br>
'$"The memory stick must be stored in a secure place such as a safe. Do not leave it in an unsecured area under any circumstances."'
<br><br>'

#Show list of servers
/opt/karoshi/web_controls/show_servers "$MOBILE" pdc $"Backup Essential Data"

echo '</form></display-karoshicontent></body></html>'
exit
