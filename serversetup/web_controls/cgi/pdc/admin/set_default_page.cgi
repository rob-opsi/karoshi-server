#!/bin/bash
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Set Default Page"
TITLEHELP=$"Choose the default page that you want to have for this section of the web management."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=7
#Assign DEFAULTPAGE
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = DEFAULTPAGEcheck ]
	then
		let COUNTER=$COUNTER+1
		DEFAULTPAGE=`echo $DATA | cut -s -d'_' -f$COUNTER-`
		break
	fi
	let COUNTER=$COUNTER+1
done

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/set_default_page_fm.cgi";
</script>
</body>
</html>'
exit
}

function completed_status {
echo '<script>
	<!--
		if (top.location!= self.location) {
			top.location = self.location.href
		}
	//-->
</script>'
echo '
<script>
	window.location = "/cgi-bin/admin/'"$DEFAULTPAGE"'";
</script>
</body></html>'
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [ `grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin` != 1 ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that DEFAULTPAGE is not blank
if [ -z "$DEFAULTPAGE" ]
then
	MESSAGE=$"The default page cannot be blank."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/set_default_page.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$DEFAULTPAGE:" | sudo -H /opt/karoshi/web_controls/exec/set_default_page
MESSAGE=$"Your default web management page has been set."
completed_status
exit
