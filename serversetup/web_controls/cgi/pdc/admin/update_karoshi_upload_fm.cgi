#!/bin/bash
#copy_files_upload
#Copyright (C) 2012  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk
#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Upload Web Management Patch"
TITLEHELP=$"This will allow you to upload and apply a web management updates to your server. You will also need the signature file to verify that it is a legitamate update."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo ' <FORM ENCTYPE="multipart/form-data" action="/cgi-bin/admin/update_karoshi_upload.cgi" method="post">

<table>
	<tr>
		<td>
			'$"Patch file"':
		</td>
		<td>
			<INPUT TYPE="FILE" NAME="file-to-upload-01" SIZE="35">
		</td>
	</tr>
	<tr>
		<td>
			'$"Sign file"':
		</td>
		<td>
			<INPUT TYPE="FILE" NAME="file-to-upload-02" SIZE="35">
		</td>
	</tr>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
'


echo '</display-karoshicontent></body></html>'
exit
