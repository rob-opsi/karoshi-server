#!/bin/bash
#Delete User
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Delete User"
TITLEHELP=$"Delete users from your system."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Delete_User"


DeleteCode="${RANDOM:0:3}"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/script.js"></script>'

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=5
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign username
DATANAME=USERNAME
get_data
USERNAME="$DATAENTRY"

#FILE
DATANAME=FILE
get_data
FILE="$DATAENTRY"

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/delete_user.cgi" method="post">
<input name="_FORMCODE_" value="'"$DeleteCode"'" type="hidden">'


#Check that this server is not part of a federated setup
if [ -f /opt/karoshi/server_network/servers/"$HOSTNAME"/federated_server ]
then
	echo $"This server is part of a federated system. Users must be deleted on the main federation server." '</div></div></body></html>'
	exit
fi


#Get request data if asked
if [ -z "$FILE" ]
then
	if [ -f /opt/karoshi/user_requests/delete_users/"$FILE" ]
		then
		NEW_USER_DATA=$(sed -n 1,1p /opt/karoshi/user_requests/delete_users/"$FILE")
		FORENAME=$(echo "$NEW_USER_DATA" | cut -d: -f1)
		SURNAME=$(echo "$NEW_USER_DATA" | cut -d: -f2)
		GROUP=$(echo "$NEW_USER_DATA" | cut -d: -f3)
		echo '<input name="_REQUESTFILE_" value="'$FILE'" type="hidden">'
		#Try and get username

		if [[ $(echo "$GROUP" | grep -c ^yr) -gt 0 ]]
		then
			GROUPCHARCOUNT=$(echo "$GROUP" | wc -c)
			let GROUPCHARCOUNT="$GROUPCHARCOUNT"-1
			let GROUPSTARTCHAR="$GROUPCHARCOUNT"-2
			USERNAME=$(echo "${FORENAME:0:1}$SURNAME${GROUP:$GROUPSTARTCHAR:$GROUPCHARCOUNT}" | tr 'A-Z' 'a-z')
		else
			USERNAME=$(echo "${FORENAME:0:1}$SURNAME" | tr 'A-Z' 'a-z')
		fi
	fi
fi


echo '
<table class="standard" style="text-align: left;" >
	<tbody>
		<tr>
			<td class="karoshi-input">
			'$"Username"'
			</td>
			<td><div id="suggestions"></div><input required="required" tabindex= "1" class="karoshi-input" name="_USERNAME_" value="'"$USERNAME"'" size="20" type="text" id="inputString" onkeyup="lookup(this.value);">
			</td>
			<td>
				<a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Delete_User"><span class="icon-large-tooltip">'$"Please enter the username that you want to delete. This WILL delete all of the user files and their home folder."'</span></a>
			</td>
		</tr>
		<tr>
			<td>'$"Confirm"'</td>
			<td style="vertical-align: top; text-align: left;"><input class="karoshi-input" min="0" max="999" required="required" tabindex= "2" name="_SHUTDOWNCODE_" maxlength="3" size="3" type="number"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Delete_User"><span class="icon-large-tooltip">'$"Type in the number displayed to confirm that you want to delete the user."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Delete Code"'</td>
			<td style="vertical-align: top; text-align: left;"><b>'"$DeleteCode"'</b></td><td></td>
		</tr>
	 	<tr>
			<td>'$"Archive home area"'</td>
			<td><input id="ArchiveHomeArea" type="checkbox" name="_ARCHIVE_" value="yes"><label for="ArchiveHomeArea"></label></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Delete_User"><span class="icon-large-tooltip">'$"This will archive the user home area to the archive folder in /home/users."'</span></a></td>
		</tr>
		<tr><td style="vertical-align:top">'$"User Photo"'</td><td><div style="width: 120px;" id="photobox"><img src="/images/blank_user_image.jpg" width="120" height="150" alt="photo"></div></td><td></td></tr>
	</tbody>
</table>
<br><input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset"></div>'

echo '
</display-karoshicontent>
</body></html>'
exit

