#!/bin/bash
#Copyright (C) 2008  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Enable Quotas on Partitions"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser
source /opt/karoshi/web_controls/version


echo '
<form action="/cgi-bin/admin/quotas_enable.cgi" name="selectservers" method="post">

<button class="button" formaction="quotas_view_partitions.cgi" name="_EnabledPartitions_" value="_">
'$"Enabled Partitions"'
</button>

<button class="button" formaction="quotas_view_usage_fm.cgi" name="_QuotaUsage_" value="_">
'$"Quota Usage"'
</button>

<button class="button" formaction="quotas_set_fm.cgi" name="_QuotaSettings_" value="_">
'$"Quota Settings"'
</button>
<br><br>
'$"Choose the server that you want to enable the quotas on."' <a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"This will enable quotas on the /home partition of the chosen server."'</span></a><br><br>'

#Show list of servers
/opt/karoshi/web_controls/show_servers $MOBILE servers $"Enable quotas"

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
