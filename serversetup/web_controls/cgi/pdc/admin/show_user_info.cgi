#!/bin/bash
#Change a user's password
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Edit User Information"
TITLEHELP=$"This will show the LDAP information for that user."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Edit_User"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')
#########################
#Assign data to variables
#########################
END_POINT=12
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign USERNAME
DATANAME=USERNAME
get_data
USERNAME=$(echo "$DATAENTRY" | tr -cd '%A-Za-z0-9.' | tr '[:upper:]' '[:lower:]')

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

#Assign SERVERTYPE
DATANAME=SERVERTYPE
get_data
SERVERTYPE="$DATAENTRY"

#Assign SERVERMASTER
DATANAME=SERVERMASTER
get_data
SERVERMASTER="$DATAENTRY"

function show_status {
echo '
	<SCRIPT language="Javascript">
		alert("'"$MESSAGE"'");
		window.location = "/cgi-bin/admin/show_user_info_fm.cgi";
	</script>
	</display-karoshicontent>
</body></html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that username is not blank
if [ -z "$USERNAME" ]
then
	MESSAGE=$"The username must not be blank."
	show_status
fi

#Check to see that SERVERNAME is not blank
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"The servername cannot be blank."
	show_status
fi

#Check to see that SERVERTYPE is not blank
if [ -z "$SERVERTYPE" ]
then
	MESSAGE=$"The servertype cannot be blank."
	show_status
fi

#Check to see that SERVERMASTER is not blank
if [ "$SERVERTYPE" = federatedslave ]
then
	if [ -z "$SERVERMASTER" ]
	then
		MESSAGE=$"The servermaster cannot be blank."
		show_status
	fi
fi

#Check to see that the user exists
echo "$Checksum:$USERNAME" | sudo -H /opt/karoshi/web_controls/exec/existcheck_user
if [ "$?" = 111 ]
then
	MESSAGE=$"This username does not exist."
	show_status
fi

#Check that there are no spaces in the password
#if [ `echo $PASSWORD1 | grep -c +` != 0 ]
#then
#MESSAGE=$"The display name name cannot be blank."
#show_status
#fi
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/show_user_info.cgi | cut -d' ' -f1)

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
		<form action="/cgi-bin/admin/show_user_info_fm.cgi" method="post">
			<button class="button" name="_ChooseUser_" value="_">
				'$"Select user"'
			</button>

			<button formaction="/cgi-bin/admin/group_membership.cgi" class="button" name="____GroupMembership____" value="____USERNAME____'"$USERNAME"'____">
				'$"Group Membership"'
			</button>


			<button formaction="/cgi-bin/admin/default_user_settings_fm.cgi" class="button" name="_DefaultUserSettings_" value="_">
				'$"Default Settings"'
			</button>
		</form>
'

echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$USERNAME:$SERVERNAME:$SERVERTYPE:$SERVERMASTER:" | sudo -H /opt/karoshi/web_controls/exec/show_user_info
echo '</display-karoshicontent></body></html>'
exit
