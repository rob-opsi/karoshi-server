#!/bin/bash
#Copyright (C) 2015  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLEHELP=$"Gluster Volumes"
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Gluster_Volumes"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+*')

#########################
#Assign data to variables
#########################
END_POINT=24
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign VOLUME
DATANAME=VOLUME
get_data
VOLUME="$DATAENTRY"

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign SERVER
DATANAME=SERVER
get_data
SERVER="$DATAENTRY"

#Assign SERVERS
DATANAME=SERVERS
get_data
SERVERS="$DATAENTRY"

#Assign FOLDER
DATANAME=FOLDER
get_data
FOLDER="$DATAENTRY"

TITLE="View Gluster Volumes"
[ "$ACTION" = create ] && TITLE=$"Create Gluster Volume"
[ "$ACTION" = reallycreate ] && TITLE=$"Creating Gluster Volume"
[ "$ACTION" = addfolder ] && TITLE=$"Add Folder"
[ "$ACTION" = reallyaddfolder ] && TITLE=$"Ading Folder"
[ "$ACTION" = assignshare ] && TITLE=$"Assign Network Share"
[ "$ACTION" = removefolder ] && TITLE=$"Remove Folder"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/gluster_control.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

[ -z "$ACTION" ] && ACTION=view
ACTION2=create
ACTIONMSG=$"Create Volume"
if [ "$ACTION" = create ] || [ "$ACTION" = addfolder ] || [ "$ACTION" = assignshare ] || [ "$ACTION" = removefolder ] || [ "$ACTION" = confirmremovefolder ] || [ "$ACTION" = reallyremovefolder ] || [ "$ACTION" = deleteglustervolume ] || [ "$ACTION" = status ] || [ "$ACTION" = restorebrick ]  
then
	ACTION2=view
	ACTIONMSG=$"View Volumes"
fi
#########################
#Check data
#########################

if [ "$ACTION" != create ] && [ "$ACTION" != reallycreate ] && [ "$ACTION" != restore ] && [ "$ACTION" != view ] && [ "$ACTION" != addfolder ] && [ "$ACTION" != reallyaddfolder ] && [ "$ACTION" != assignhomefolders ] && [ "$ACTION" != removefolder ] && [ "$ACTION" != reallyremovefolder ] && [ "$ACTION" != confirmremovefolder ] && [ "$ACTION" != deleteglustervolume ] && [ "$ACTION" != status ] && [ "$ACTION" != restorebrick ] && [ "$ACTION" != StartForced ]
then
	MESSAGE=$"You have not entered a correct action."
	show_status
fi

if [ "$ACTION" = reallycreate ]
then
	if [ -z "$VOLUME" ]
	then
		MESSAGE=$"You have not entered a volume name."
		show_status
	fi
	if [ -z "$SERVERS" ]
	then
		MESSAGE=$"You have not chosen any servers."
		show_status
	fi
	if [[ $(echo "$SERVERS" | sed 's/%2C/,/g' | grep -c ",") = 0 ]]
	then
		MESSAGE=$"You have to choose at least two servers to create a distributed volume."
		show_status
	fi

	if [ -d /opt/karoshi/server_network/gluster-volumes/"$VOLUME" ]
	then
		MESSAGE=''$VOLUME' - '$"This volume has already been created."''
		show_status
	fi
fi

if [ "$ACTION" = reallyaddfolder ]
then
	if [ -z "$VOLUME" ]
	then
		MESSAGE=$"You have not entered a volume name."
		show_status
	fi
	if [ -z "$FOLDER" ]
	then
		MESSAGE=$"You have not chosen a folder path."
		show_status
	fi	
fi


echo '
<form action="/cgi-bin/admin/gluster_control.cgi" method="post">
	<button class="button" name="_GlusterAction_" value="_ACTION_'"$ACTION2"'_">
		'"$ACTIONMSG"'
	</button>

	<button class="button" formaction="home_folders_fm.cgi" name="ViewHomeFolders" value="_">
		'$"Home Folders"'
	</button>

	<button class="button" formaction="samba_shares.cgi" name="NetworkShares" value="_">
		'$"Network Shares"'
	</button>
</form>
<form action="/cgi-bin/admin/gluster_control.cgi" method="post" id="combobox" name="combobox">'
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/gluster_control.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$VOLUME:$SERVER:$SERVERS:$FOLDER:$MOBILE:" | sudo -H /opt/karoshi/web_controls/exec/gluster_control
echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
