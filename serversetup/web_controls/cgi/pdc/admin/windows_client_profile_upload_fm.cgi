#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Windows Profile - Upload"
TITLEHELP=$"Allows you to upload a new profile that can be used as a mandatory profile for users."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Upload_a_new_profile"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<P>
'$"Select the compressed profile that you want to upload in .zip or .tar.gz format."':
<P>
<FORM ENCTYPE="multipart/form-data" ACTION="/cgi-bin/admin/windows_client_profile_upload.cgi" METHOD="POST">
	<table>
        	<tr>
			<td class="karoshi-input">'$"Compressed profile"':</td>
			<td><INPUT TYPE="FILE" NAME="file-to-upload-01" SIZE="35"></td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Upload_a_new_profile"><span class="icon-large-tooltip">'$"You need to upload a zip or tar.gz file of your chosen profile."'<br><br>'$"There should be one folder in the top level of the created archive which is the name of your profile."'</span></a></td>
		</tr>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</body>
</html>'
exit
