#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Technical Support Action Request"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\.%+_:\-')
#########################
#Assign data to variables
#########################
END_POINT=13
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign FEEDBACK
DATANAME=FEEDBACK
get_data
FEEDBACK="$DATAENTRY"

#Assign PRIORITY
DATANAME=PRIORITY
get_data
PRIORITY="$DATAENTRY"

#Assign ASSIGNED
DATANAME=ASSIGNED
get_data
ASSIGNED="$DATAENTRY"

#Assign ASSIGNED2
DATANAME=ASSIGNED2
get_data
ASSIGNED2="$DATAENTRY"

#Assign JOBNAME
DATANAME=JOBNAME
get_data
JOBNAME="$DATAENTRY"

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
window.location = "/cgi-bin/admin/helpdesk_view_fm.cgi";
</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi

#########################
#Check data
#########################
#Check that ACTION is not blank
if [ -z "$ACTION" ]
then
	MESSAGE=$"The action cannot be blank."
	show_status
fi

if [ $ACTION = update ]
then
#Check to see that ASSIGNED is not blank
if [ -z "$ASSIGNED" ] && [ -z "$ASSIGNED2" ]
then
	MESSAGE=$"You have not assigned anyone to this request."
	show_status
fi
#Check to see that PRIORITY is not blank
if [ -z "$PRIORITY" ]
then
	MESSAGE=$"The priority must not be blank."
	show_status
fi

#Check that JOBNAME is not blank
if [ -z "$JOBNAME" ]
then
	MESSAGE=$"The job name cannot be blank."
	show_status
fi

[ ! -z "$ASSIGNED2" ] && ASSIGNED="$ASSIGNED2"

#Convert special characters
ASSIGNED=`echo $ASSIGNED | sed 's/%0D%0A/ /g' | sed 's/+/ /g' | sed 's/%27//g'  | sed 's/%22//g'`
PRIORITY=`echo $PRIORITY | sed 's/%0D%0A/ /g' | sed 's/+/ /g' | sed 's/%27//g'  | sed 's/%22//g' | sed 's/%24/$/g' | sed 's/%2C/,/g' | sed 's/%21/!/g'`
FEEDBACK=`echo $FEEDBACK | sed 's/%0D%0A/ /g' | sed 's/+/ /g' | sed 's/%27//g'  | sed 's/%22//g' | sed 's/%3F/?/g' | sed 's/%2C/,/g' | sed 's/%21/!/g' | sed 's/%2F/\//g' | sed 's/%28/(/g' | sed 's/%29/)/g'`

fi
[ ! -d /opt/karoshi/server_network/helpdesk/log ] && mkdir -p /opt/karoshi/server_network/helpdesk/log
LOG_DATE=$(date +%F)

if [ "$ACTION" = update ]
then
	echo `date`: Helpdesk - $JOBNAME updated by $REMOTE_USER from $REMOTE_ADDR >> "/opt/karoshi/server_network/helpdesk/log/$LOG_DATE"
	#Update request
	#Assigned
	sed -i 9c\ASSIGNED='"'"$ASSIGNED"'"' "/opt/karoshi/server_network/helpdesk/todo/$JOBNAME"
	#Priority
	sed -i 10c\PRIORITY='"'"$PRIORITY"'"' "/opt/karoshi/server_network/helpdesk/todo/$JOBNAME"
	#Feedback
	sed -i 11c\FEEDBACK='"'"$FEEDBACK"'"' "/opt/karoshi/server_network/helpdesk/todo/$JOBNAME"
fi

if [ "$ACTION" = delete ]
then
echo `date`: Helpdesk - deleting $JOBNAME by $REMOTE_USER from $REMOTE_ADDR >> "/opt/karoshi/server_network/helpdesk/log/$LOG_DATE"
[ ! -d /opt/karoshi/server_network/helpdesk/deleted ] && mkdir -p /opt/karoshi/server_network/helpdesk/deleted
[ -f "/opt/karoshi/server_network/helpdesk/todo/$JOBNAME" ] && mv "/opt/karoshi/server_network/helpdesk/todo/$JOBNAME" /opt/karoshi/server_network/helpdesk/deleted/

fi

if [ "$ACTION" = completed ]
then
echo `date`: Helpdesk - $JOBNAME marked as complete by $REMOTE_USER from $REMOTE_ADDR >> "/opt/karoshi/server_network/helpdesk/log/$LOG_DATE"
[ ! -d /opt/karoshi/server_network/helpdesk/completed ] && mkdir -p /opt/karoshi/server_network/helpdesk/completed
[ -f "/opt/karoshi/server_network/helpdesk/todo/$JOBNAME" ] && mv "/opt/karoshi/server_network/helpdesk/todo/$JOBNAME" /opt/karoshi/server_network/helpdesk/completed/

#Add in completion dates
sed -i 12c\COMPLETEDDATE='"'`date +%d-%m-%y`'"' /opt/karoshi/server_network/helpdesk/completed/$JOBNAME
sed -i 13c\COMPLETEDDATE2='"'`date +%s`'"' /opt/karoshi/server_network/helpdesk/completed/$JOBNAME
fi

if [ "$ACTION" = notcompleted ]
then
	echo `date`: Helpdesk - $JOBNAME marked as not complete by $REMOTE_USER from $REMOTE_ADDR >> "/opt/karoshi/server_network/helpdesk/log/$LOG_DATE"
	[ -f /opt/karoshi/server_network/helpdesk/completed/$JOBNAME ] && mv /opt/karoshi/server_network/helpdesk/completed/"$JOBNAME" /opt/karoshi/server_network/helpdesk/todo/
fi

echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:no:" | sudo -H /opt/karoshi/web_controls/exec/helpdesk_warning_message
echo '
<script>
	window.location = "/cgi-bin/admin/helpdesk_view_fm.cgi";
</script>
</body>
</html>'
exit



