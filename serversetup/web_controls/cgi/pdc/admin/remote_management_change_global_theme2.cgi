#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Generate head section
source /opt/karoshi/web_controls/generate_head_section

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=5
#Assign THEMECHOICE
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = THEMECHOICEcheck ]
	then
		let COUNTER=$COUNTER+1
		THEMECHOICE=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done

function show_status {

#Generate page top
source /opt/karoshi/web_controls/generate_page_admin_top

#Generate side bar
source /opt/karoshi/web_controls/generate_sidebar_admin

#Add in scripts
/opt/karoshi/web_controls/generate_scripts

echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/remote_management_change_theme.cgi";
</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that THEMECHOICE is not blank
if [ -z "$THEMECHOICE" ]
then
	MESSAGE=$"The theme choice must not be blank."
	show_status
fi

#Check that theme choice exists
if [ ! -f /var/www/html_karoshi/responsive/assets/css/"$THEMECHOICE".css ]
then
	MESSAGE=$"This theme choice does not exist."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/remote_management_change_global_theme2.cgi | cut -d' ' -f1)
#Change language
sudo -H /opt/karoshi/web_controls/exec/remote_management_change_global_theme $REMOTE_USER:$REMOTE_ADDR:$Checksum:$THEMECHOICE
THEMESTATUS="$?"
if [ "$THEMESTATUS" = 101 ]
then
	MESSAGE=$"There was a problem changing the theme."" "$"Please check the karoshi web administration logs for more details."
	show_status
fi
echo '
</head>
<body>
<script>
	window.location = "/cgi-bin/admin/remote_management_change_global_theme.cgi";
</script>
</body>
</html>'
exit
