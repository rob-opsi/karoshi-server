#!/bin/bash
#Change password
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Change a User Password"
TITLEHELP=$"This will change the password of the user for access to all servers on the Karoshi system."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Change_Password"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/script.js"></script>'

#Get data input
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')

#Assign data to variables

END_POINT=9
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign USERNAME
DATANAME=USERNAME
get_data
USERNAME="$DATAENTRY"

#Assign PASSWORD1
DATANAME=PASSWORD1
get_data
PASSWORD1="$DATAENTRY"

#Assign PASSWORD2
DATANAME=PASSWORD2
get_data
PASSWORD2="$DATAENTRY"

#Assign NEXTLOGON
DATANAME=NEXTLOGON
get_data
NEXTLOGON="$DATAENTRY"

if [ -z "$NEXTLOGON" ]
then
	NEXTLOGON=no
fi

CHECKED=""
[ "$NEXTLOGON" = yes ] && CHECKED="checked"

#Check password settings
source /opt/karoshi/server_network/security/password_settings

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

#Page content
echo '<form action="/cgi-bin/admin/change_password.cgi" method="post">
<table>
<tbody>
<tr><td class="karoshi-input">'$"Username"'</td><td><div id="suggestions"></div>
<input required="required" tabindex= "1" class="karoshi-input" name="____USERNAME____" 
 value="'"$USERNAME"'" size="20" type="text" id="inputString" onkeyup="lookup(this.value);"></td><td>
<a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Change_Password"><span class="icon-large-tooltip">'$"Enter in the username that you want to change the password for."'</span></a>
</td></tr>
<tr>
<td>'$"New Password"'</td><td><input pattern=".{'"$MINPASSLENGTH"',128}" title="'$"Password length required:"' '"$MINPASSLENGTH"'" tabindex= "2" class="karoshi-input" name="____PASSWORD1____" value="'"$PASSWORD1"'" size="20" type="password"></td><td>
<a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Change_Password"><span class="icon-large-tooltip">'$"Enter in the new password that you want the user to have."'<br><br>'$"Leave the password fields blank if you want a random password."'<br><br>'$"The following special characters are allowed"'<br><br> space !	&quot;	# 	$	%	&amp; 	(	) 	*	+	, 	-	.	/ 	:
;	&lt;	=	&gt;	?	@ 	[	\	]	^	_	` 	{	|	}	~<br><br>'

[ "$PASSWORDCOMPLEXITY" = on ] && echo ''$"Upper and lower case characters and numbers are required."'<br><br>'
echo ''$"The Minimum password length is "''"$MINPASSLENGTH"'.<br></span></a>
</td>
</tr>
<tr><td>'$"Confirm New Password"'</td><td style="vertical-align: top;"><input pattern=".{'"$MINPASSLENGTH"',128}" title="'$"Password length required:"' '"$MINPASSLENGTH"'" tabindex= "3" class="karoshi-input" name="____PASSWORD2____" value="'"$PASSWORD2"'" size="20" type="password"></td><td></td>
      </tr>
	<tr><td style="vertical-align: top;">'$"Change at next logon"'</td><td><input id="ChangeAtNextLogon" type="checkbox" name="____NEXTLOGON____" value="yes" '"$CHECKED"'><label for="ChangeAtNextLogon"> </label></td><td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Change_Password"><span class="icon-large-tooltip">'$"This will force the user to change their password at next logon."'</span></a></td></tr>
<tr><td style="vertical-align: top;">'$"User Photo"'</td><td><div style="width: 120px;" id="photobox"><img src="/images/blank_user_image.jpg" width="120" height="150" alt="photo"></div></td><td></td></tr>
</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>'

echo '</display-karoshicontent></body></html>'

exit

