#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Update Web Management"
TITLEHELP=$"This shows any Karoshi Server patches that are available."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Update_Linux_Schools_Server_System"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=7
#Assign ACTION
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = ACTIONcheck ]
	then
		let COUNTER=$COUNTER+1
		ACTION=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done
[ -z "$ACTION" ] && ACTION=UPDATES

#Show update list choice
if [ "$ACTION" = ALL ]
then
	UPDATELIST=updatelist_all.html
	[ "$MOBILE" = yes ] && UPDATELIST=updatelist_all_mobile.html
	ICON=/images/submenus/system/updates.png
	ACTION=UPDATES
	MESSAGE=$"Available updates"
else
	UPDATELIST=updatelist.html
	[ "$MOBILE" = yes ] && UPDATELIST=updatelist_mobile.html
	ICON=/images/submenus/system/updates_all.png
	ACTION=ALL
	MESSAGE=$"Installed Updates"
fi

echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '	
	<form action="/cgi-bin/admin/update_karoshi_fm.cgi" name="selectservers" method="post">	
		<button formaction="/cgi-bin/admin/update_karoshi_fm.cgi" class="button" name="Updates" value="_ACTION_'"$ACTION"'_">
			'"$MESSAGE"'
		</button>
	
		<button formaction="/cgi-bin/admin/refresh_karoshi_update_list.cgi" class="button" name="Refresh" value="_">
			'$"Check for updates"'
		</button>
	</form>
	'

echo '<form action="/cgi-bin/admin/update_karoshi.cgi" name="selectservers" method="post">'

if [ -f /opt/karoshi/updates/"$UPDATELIST" ]
then
	cat /opt/karoshi/updates/"$UPDATELIST"
else
	echo $"No updates are available."
fi

echo '
</form>
</display-karoshicontent>
</body>
</html>'

exit
