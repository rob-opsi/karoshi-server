#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi SERVERNAME.
#
#Karoshi SERVERNAME is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi SERVERNAME is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi SERVERNAME.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser

source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"View Network Backup Logs"
TITLEHELP=$"Choose the server that you want to view the backup logs for."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=View_Backup_Logs"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'
#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=22
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

#Assign DATE
DATANAME=DATE
get_data
DATE=$(echo "$DATAENTRY" | tr -cd '0-9-')

#Assign LOGTYPE
DATANAME=LOGTYPE
get_data
LOGTYPE="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/backup_view_logs_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that a SERVERNAME has been chosen
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"You must choose a server to enable or disable."
	show_status
fi

#Check to see that DATE is not blank
if [ -z "$DATE" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi

if [ -z "$LOGTYPE" ]
then
	LOGTYPE=summary
fi

#Check that date is valid
DAY=$(echo $DATE | cut -d- -f1)
MONTH=$(echo $DATE | cut -d- -f2)
YEAR=$(echo $DATE | cut -d- -f3)

#Check to see that DAY, MONTH or YEAR is not blank
if [ -z "$DAY" ] || [ -z "$MONTH" ] || [ -z "$YEAR" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi

if [ "$DAY" -gt 31 ]
then
	MESSAGE=$"Incorrect date format."
	show_status
fi

if [ "$MONTH" -gt 12 ]
then
	MESSAGE=$"Incorrect date format."
	show_status
fi

if [ "$YEAR" -lt 2006 ] || [ "$YEAR" -gt 3006 ]
then
	MESSAGE=$"Incorrect date format."
	show_status
fi

WIDTH=100
ICON1=/images/submenus/system/logs.png
ICON2=/images/submenus/system/logs.png
ICON3=/images/submenus/system/computer.png

echo '<form action="/cgi-bin/admin/backup_view_logs.cgi" method="post">'

if [ "$LOGTYPE" = summary ]
then
	echo '
	<button class="button" name="_ViewDetailedLogs_" value="_SERVERNAME_'"$SERVERNAME"'_LOGTYPE_detailed_DATE_'"$DATE"'_">
		'$"Detailed logs"'
	</button>'
else
	echo '
	<button class="button" name="_ViewSummaryLogs_" value="_LOGTYPE_summary_DATE_'"$DATE"'_">
			'$"Summary logs"'
	</button>'
fi

echo '
	<button formaction="/cgi-bin/admin/backup_view_logs_fm.cgi" class="button" name="SelectServer" value="_">
			'$"Select Server"'
	</button>
</form>
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">Server name</td>
			<td>'"$SERVERNAME"'</td>
		</tr>
		<tr>
			<td>'$"Backup logs"'</td>
			<td>'"$DAY-$MONTH-$YEAR"'</td>
		</tr>
	</tbody>
</table>
'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/backup_view_logs.cgi | cut -d' ' -f1)
#View logs
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$DAY:$MONTH:$YEAR:$SERVERNAME:$LOGTYPE:" | sudo -H /opt/karoshi/web_controls/exec/backup_view_logs
echo '
</display-karoshicontent>
</body>
</html>'
exit

