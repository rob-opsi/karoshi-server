#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Change Timeout"
USER_TIMEOUT=$TIMEOUT

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
				<display-karoshicontent>
				<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
				<form action="/cgi-bin/admin/remote_management_change_timeout.cgi" method="post">
				<table>
					<tbody>
						<tr>
							<td class="karoshi-input">'$"Timeout"'</td>'
#Convert timeout to minutes
let USER_TIMEOUT="$USER_TIMEOUT"/60
echo '
							<td><input required="required" size="2" maxlength="2" name="_TIMEOUT_" value="'$USER_TIMEOUT'"></td>
							<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"This is the time delay before you are logged out of the Karoshi Web Management system."'<br><br>'$"Enter a time in minutes between 1 and 99. This is a per user setting and will not affect other users."'<br><br>'$"The default value is 5 minutes."'</span></a></td>
						</tr>
						<tr>
							<td>'$"Extend timeout from"'</td>
							<td><input required="required" size="15" maxlength="15" name="_NOTIMEOUT_" value="'$NOTIMEOUT'"></td>
							<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Choose a tcpip number or range to extend the timeout from. The timeout setting will extend to 1 day for computers matching this value."'<br><br>'$"Example1 - 172.30.3.1 - individual computer"'<br><br>'$"Example2 - 172.30.3 - all computers in this range"'</span></a></td>
						</tr>
					</tbody>
				</table>
				<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
				</form>
		</display-karoshicontent>
	</body>
</html>
'
exit
