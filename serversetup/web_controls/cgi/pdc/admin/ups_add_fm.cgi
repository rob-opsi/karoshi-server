#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Add a UPS"
TITLEHELP=$"This will configure a UPS device connected to a server."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_a_UPS"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/ups_status.cgi" name="tstest" method="post">
	<button class="button" name="_UPSStatus" value="_">
		'$"Status"'
	</button>
</form>
<form action="/cgi-bin/admin/ups_add.cgi" name="tstest" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"UPS Name"'</td>
				<td>
					<select name="_UPSDRIVER_" class="karoshi-input">'
#Generate list of UPC data
UPSDATA_LENGTH=$(wc -l < /opt/karoshi/server_network/ups/ups-model-information.csv)

COUNTER=1
while [ "$COUNTER" -le "$UPSDATA_LENGTH" ]
do
	UPSDATA=$(sed -n "$COUNTER,$COUNTER"'p' /opt/karoshi/server_network/ups/ups-model-information.csv)
	UPSMAKE=$(echo "$UPSDATA" | cut -d, -f1,2 | sed 's/,/: /g')
	UPSMODEL=$(echo "$UPSDATA" | cut -d, -f2)
	UPSDRIVER=$(echo "$UPSDATA" | cut -d, -f3)
	echo '
						<option value="'"$UPSMODEL,$UPSDRIVER"'">'"$UPSMAKE"'</option>'
	let COUNTER="$COUNTER"+1
done

echo '
					</select>
				</td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_a_UPS"><span class="icon-large-tooltip">'$"Choose a UPS device from the list."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Port"'</td>
				<td>
					<select class="karoshi-input" name="_UPSPORT_">
						<option value="auto">auto</option>
						<option value="/dev/ttyS0">/dev/ttyS0</option>
						<option value="/dev/ttyS1">/dev/ttyS1</option>
					</select>
				</td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Add_a_UPS"><span class="icon-large-tooltip">'$"Choose auto for UPS devices connected via usb."'</span></a></td>
			</tr>
		</tbody>
	</table>'

#Show list of servers
/opt/karoshi/web_controls/show_servers "$MOBILE" servers $"Add UPS"

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit

