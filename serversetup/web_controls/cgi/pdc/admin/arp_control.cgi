#!/bin/bash
#Copyright (C) 2016  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"ARP Control"
TITLEHELP=$"This allows you to set static ARP entries for all of your servers to protect against ARP poisoning attacks."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Arp_Controls"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


DATA=$(cat | tr -cd 'A-Za-z0-9\._:\/*%+"-' | sed 's/____/QUADUNDERSCORE/g' | sed 's/_/UNDERSCORE/g' | sed 's/QUADUNDERSCORE/_/g')
#########################
#Assign data to variables
#########################
END_POINT=15
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign DEVICE
DATANAME=DEVICE
get_data
DEVICE="${DATAENTRY//UNDERSCORE/_}"

#Assign MACADDR
DATANAME=MACADDR
get_data
MACADDR="$DATAENTRY"

function show_status {
echo '<script>
alert("'"$MESSAGE"'");
window.location = "/cgi-bin/admin/arp_control.cgi";
</script></display-karoshicontent></body></html>'
exit
}

if [ -z "$ACTION" ]
then
	ACTION=view
fi

#Check data

if [ "$ACTION" = static ] || [ "$ACTION" = dynamic ]
then
	if [ -z "$DEVICE" ]
	then
		MESSAGE=$"No TCPIP information has been received."
		show_status
	fi
	if [ -z "$MACADDR" ]
	then
		MESSAGE=$"No MAC address has been received."
		show_status
	fi

	#Check that tcpip number is correct
	#Check that we have 4 dots
	if [[ $(echo "$DEVICE" | sed 's/\./\n /g'  | sed /^$/d | wc -l) != 4 ]]
	then
		MESSAGE=$"The TCPIP number is not corrrect."
		show_status
	fi
	#Check that no number is greater than 255
	HIGHESTNUMBER=$(echo "$DEVICE" | sed 's/\./\n /g'  | sed /^$/d | sort -g -r | sed -n 1,1p)
	if [ "$HIGHESTNUMBER" -gt 255 ]
	then
		MESSAGE=$"The TCPIP number is not corrrect."
		show_status
	fi

	#Check that the mac address is correct
	#Check that we have 6 sets of data
	MACADDR2="${MACADDR//%3A/:}"

	if [[ $(echo "$MACADDR2" | sed 's/:/\n/g' | wc -l) != 6 ]]
	then
		echo "count is:"
		echo "$MACADDR2" | sed 's/:/\n/g' | wc -l
		echo "<br>"
		MESSAGE=''$"You have not entered in a valid mac address1."''
		show_status	
	fi
	#Check max chars
	for LINEDATA in ${MACADDR2//:/ }
	do
		if [[ $(echo "$LINEDATA" | wc -L) != 2 ]]
		then
			MESSAGE=''$"You have not entered in a valid mac address2."''
			show_status
		fi
	done
fi



echo '
<img src="/images/help/warning.png" alt="">
'$"Be careful using this feature."' '$"If you need to change a network card remove the mac address from your static arps first."'
<br>
<form action="/cgi-bin/admin/arp_control.cgi" method="post">'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/arp_control.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$DEVICE:$MACADDR" | sudo -H /opt/karoshi/web_controls/exec/arp_control

echo '</form>
</display-karoshicontent>
</body>
</html>'
exit
