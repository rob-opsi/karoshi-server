#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Restrict Access"
TITLEHELP=$"Access can be restricted by a complete tcpip number or by a partial number for a range."' '$"For example 172.30.4. will allow all computers from 172.30.4.1 to 172.30.4.254."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Change_Password"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\%+-')
#########################
#Assign data to variables
#########################
END_POINT=10
#Assign _TCPADDRESS_
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
if [ `echo $DATAHEADER'check'` = TCPADDRESScheck ]
then
let COUNTER=$COUNTER+1
TCPADDRESS=`echo $DATA | cut -s -d'_' -f$COUNTER`
break
fi
let COUNTER=$COUNTER+1
done
#Assign _TCPCOMMENT_
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
if [ `echo $DATAHEADER'check'` = TCPCOMMENTcheck ]
then
let COUNTER=$COUNTER+1
TCPCOMMENT=`echo $DATA | cut -s -d'_' -f$COUNTER`
break
fi
let COUNTER=$COUNTER+1
done
#Assign _CURRENTTCPIP_
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
if [ `echo $DATAHEADER'check'` = CURRENTTCPIPcheck ]
then
let COUNTER=$COUNTER+1
CURRENTTCPIP=`echo $DATA | cut -s -d'_' -f$COUNTER`
break
fi
let COUNTER=$COUNTER+1
done
#Assign _ADMINLEVEL_
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
if [ `echo $DATAHEADER'check'` = ADMINLEVELcheck ]
then
let COUNTER=$COUNTER+1
ADMINLEVEL=`echo $DATA | cut -s -d'_' -f$COUNTER`
ADMINLEVEL=`echo $ADMINLEVEL | sed 's/%2B/+/g'`
break
fi
let COUNTER=$COUNTER+1
done

function view_tcpip {
echo '<SCRIPT language="Javascript">'
echo '                window.location = "/cgi-bin/admin/remote_management_restrict.cgi";'
echo '</script>'
echo "</body></html>"
exit
}

function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'$MESSAGE'")';
echo '                window.location = "/cgi-bin/admin/remote_management_restrict.cgi";'
echo '</script>'
echo "</body></html>"
exit
}
#########################
#Check https access
#########################
if [ https_$HTTPS != https_on ]
then
	MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ $REMOTE_USER'null' = null ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [ `grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin` != 1 ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
if [ $TCPADDRESS'null' = null ]
then
	MESSAGE=$"The Action must not be blank."
	show_status
fi
if [ $CURRENTTCPIP'null' = null ]
then
	MESSAGE=$"The Action must not be blank."
	show_status
fi

Checksum=`sha256sum /var/www/cgi-bin_karoshi/admin/remote_management_restrict3.cgi | cut -d' ' -f1`
sudo -H /opt/karoshi/web_controls/exec/remote_management_restrict $REMOTE_USER:$REMOTE_ADDR:$Checksum:remove:$CURRENTTCPIP::$ADMINLEVEL
sudo -H /opt/karoshi/web_controls/exec/remote_management_restrict $REMOTE_USER:$REMOTE_ADDR:$Checksum:add:$TCPADDRESS:$TCPCOMMENT:$ADMINLEVEL

view_tcpip
echo "</body></html>"
exit
