#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Schedule Wake on Lan"
TITLEHELP=$"This will schedule all of the computers in your selected location to be turned on."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Wake_on_LAN"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=7
#Assign HOUR
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = HOURcheck ]
	then
		let COUNTER=$COUNTER+1
		HOUR=`echo $DATA | cut -s -d'_' -f$COUNTER | tr -cd '0-9\n'`
		break
	fi
	let COUNTER=$COUNTER+1
done
#Assign MINUTES
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = MINUTEScheck ]
	then
		let COUNTER=$COUNTER+1
		MINUTES=`echo $DATA | cut -s -d'_' -f$COUNTER | tr -cd '0-9\n'`
		break
	fi
	let COUNTER=$COUNTER+1
done
#Assign LOCATION
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = LOCATIONcheck ]
	then
		let COUNTER=$COUNTER+1
		LOCATION=`echo $DATA | cut -s -d'_' -f$COUNTER`
		break
	fi
	let COUNTER=$COUNTER+1
done


function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/wake_on_lan_add.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_$HTTPS != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that HOUR is not blank
if [ -z "$HOUR" ]
then
	MESSAGE=$"The hour cannot be blank."
	show_status
fi
#Check to see that MINUTES is not blank
if [ -z "$MINUTES" ]
then
	MESSAGE=$"The minutes cannot be blank."
	show_status
fi
#Check to see that LOCATION fields are not blank
if [ -z "$LOCATION" ]
then
	MESSAGE=$"The location cannot be blank."
	show_status
fi
#Check that hour and minutes are correct
if [ "$HOUR" -gt 24 ]
then
	MESSAGE=$"The time has not be entered correctly."
	show_status
fi
if [ "$MINUTES" -gt 60 ]
then
	MESSAGE=$"The time has not be entered correctly."
	show_status
fi
#Add in wake on lan
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/wake_on_lan_add.cgi | cut -d' ' -f1)
sudo -H /opt/karoshi/web_controls/exec/wake_on_lan_add "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$HOUR:$MINUTES:$LOCATION"
if [ "$?" = 0 ]
then
	MESSAGE="$LOCATION - "$"Wake on Lan Schedule completed."
else
	MESSAGE=$"There was an error scheduling this job. Please check that Karoshi Web Management logs."
fi
show_status
exit
