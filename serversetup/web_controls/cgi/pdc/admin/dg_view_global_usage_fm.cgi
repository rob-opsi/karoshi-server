#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Global Internet Usage"
TITLEHELP=$"Internet Usage logs are updated at the end of every day."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/calendar2/calendar_eu.js"></script>
        <!-- Timestamp input popup (European Format) -->
<link rel="stylesheet" href="/all/calendar2/calendar.css">
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '<form action="/cgi-bin/admin/dg_view_global_usage.cgi" name="testform" method="post">

<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Log Date"'</td>
			<td>'
echo "
				<!-- calendar attaches to existing form element -->
				<input class=\"karoshi-input\" type=\"text\" value=\"$LOG_DATE\" size=14 maxlength=10 name=\"_DATE_\">
			</td>
			<td style=\"vertical-align: top;\">
				<script>
				new tcal ({
					// form name
					'formname': 'testform',
					// input name
					'controlname': '_DATE_'
				});

				</script>
			</td>
		</tr>"



echo '
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>
'
exit
