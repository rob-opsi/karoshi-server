#!/bin/bash
#Copyright (C) 2009  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"DNS"
TITLEHELP=$"This allows you to view, edit, and delete the local dns entries on your system."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=DNS"
TOOLTIPS=inforight

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-+-')
#########################
#Assign data to variables
#########################
END_POINT=19
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVERNAME="$DATAENTRY"

#Assign SERVERTYPE
DATANAME=SERVERTYPE
get_data
SERVERTYPE="$DATAENTRY"

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign NAME
DATANAME=NAME
get_data
NAME="$DATAENTRY"

#Assign DNSENTRY
DATANAME=DNSENTRY
get_data
DNSENTRY="$DATAENTRY"

#Assign DNSTYPE
DATANAME=DNSTYPE
get_data
DNSTYPE="$DATAENTRY"

#Assign ZONE
DATANAME=ZONE
get_data
ZONE="$DATAENTRY"

function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'"$MESSAGE"'")';
echo '                window.location = "/cgi-bin/admin/dnsview_fm.cgi";'
echo '</script>'
echo "</display-karoshicontent></body></html>"
exit
}

function show_dns {
echo "
<form action=\"/cgi-bin/admin/dnsview.cgi\" method=\"post\" id=\"showdns\">
<input type=\"hidden\" name=\"_SERVERNAME_$SERVERNAME""_SERVERTYPE_$SERVERTYPE""_ACTION_$ACTION""_ZONE_$ZONE""_\" value=''>
</form>
<script language=\"JavaScript\" type=\"text/javascript\">
<!--
document.getElementById('showdns').submit();
//-->
</script>
</display-karoshicontent>/body></html>
"
exit
}

#Check to see that SERVERNAME is not blank
if [ -z "$SERVERNAME" ]
then
	SERVERNAME=$(hostname-fqdn)
fi

#Check to see that SERVERTYPE is not blank
if [ -z "$SERVERTYPE" ]
then
	SERVERTYPE=network
fi

#Set a default zone
if [ -z "$ZONE" ]
then
	source /opt/karoshi/server_network/domain_information/domain_name
	ZONE="$REALM"
fi

#Check to see that action is not blank.
[ -z "$ACTION" ] && ACTION=view

#Check to see that linenumber is not blank.
[ -z "$LINENUMBER" ] && LINENUMBER=notset



TITLE=$"View Entries"
ALTTITLE=$"View Entries"
ALTDESC=$"View DNS Entries"
ACTION2=view
WIDTH=100
ICON1=/images/submenus/system/dns.png
ICON2=/images/submenus/system/dns.png

[ "$ACTION" = edit ] && TITLE=$"Edit an Entry"
[ "$ACTION" = add ] && TITLE=$"Add Entry"
[ "$ACTION" = viewdnszones ] && TITLE=$"View Zones"
[ "$ACTION" = adddzone ] && TITLE=$"Add Zone"
[ "$ACTION" = deletezone ] && TITLE=$"Delete Zone"

if [ "$ACTION" = view ]
then
	ALTTITLE=$"Add Entry"
	ALTDESC=$"Add a DNS Entry"
	ACTION2=add
	ICON2=/images/submenus/system/dnsadd.png
fi

ACTION3=viewdnszones
ICON3=/images/submenus/system/zones.png
ALTTITLE3=$"Zones"
ALTDESC3=$"View DNS Zones"
if [ "$ACTION" = viewdnszones ]
then
	ALTTITLE3=$"Add Zone"
	ALTDESC3=$"Add a DNS Zone"
	ACTION3=addzone
	ICON3=/images/submenus/system/add.png
fi

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter({
	headers: {
	2: { sorter: "ipAddress" }
    		}
		});
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
	<form action="/cgi-bin/admin/dnsview.cgi" method="post">
		<button class="button" name="_AltAction_" value="_SERVERNAME_'"$SERVERNAME"'_SERVERTYPE_'"$SERVERTYPE"'_ACTION_'"$ACTION3"'_ZONE_'"$ZONE"'_">
			'"$ALTTITLE3"'
		</button>

		<button formaction="/cgi-bin/admin/dnsview.cgi" class="button" name="_AltAction_" value="_SERVERNAME_'"$SERVERNAME"'_SERVERTYPE_'"$SERVERTYPE"'_ACTION_'"$ACTION2"'_ZONE_'"$ZONE"'_">
			'"$ALTTITLE"'
		</button>

		<button formaction="/cgi-bin/admin/dns_settings.cgi" method="post" class="button" name="_ViewDNSSettings_" value="_">
			'$"DNS Settings"'
		</button>
	</form>'


Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/dnsview.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$SERVERNAME:$SERVERTYPE:$ACTION:$NAME:$DNSENTRY:$DNSTYPE:$ZONE:$MOBILE" | sudo -H /opt/karoshi/web_controls/exec/dnsview

if [ "$ACTION" = reallyedit ] || [ "$ACTION" = reallydelete ] || [ "$ACTION" = reallyadd ] || [ "$ACTION" = reallyaddzone ] || [ "$ACTION" = reallydeletezone ]
then
	if [ "$ACTION" = reallyaddzone ] || [ "$ACTION" = reallydeletezone ]
	then
		ACTION=viewdnszones
	else
		ACTION=view
	fi
	show_dns
fi

echo '</display-karoshicontent></body></html>'
exit

