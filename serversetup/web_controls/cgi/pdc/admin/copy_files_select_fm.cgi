#!/bin/bash
#copy_files_select
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Copy Files"
TITLEHELP=$"This will copy files into all of the user home areas for the group that you choose."
HELPURL="https://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Copy_Files"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


#Check to see if any files have been uploaded
if [[ $(ls -1 /var/www/karoshi/web_upload | wc -l) = 0 ]]
then
	echo ''$"No files have been uploaded to copy to user accounts."'
	</display-karoshicontent></body></html>'
fi


echo '
	<form action="/cgi-bin/admin/copy_files_select.cgi" method="post">
		<table>
    		<tbody>
     	 		<tr>
				<td class="karoshi-input">'$"Group"'</td>
				<td>'
/opt/karoshi/web_controls/group_dropdown_list
echo '
				</td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=copy_files"><span class="icon-large-tooltip">'$"Select the group that you want to copy the data to."'</span></a></td>
			<tr>
		</tbody>
		</table>
			<br>
		<input value="'$"Submit"'" class="button primary" type="submit">
		</form>
	</display-karoshicontent>
</body>
</html>
'
fi
exit
