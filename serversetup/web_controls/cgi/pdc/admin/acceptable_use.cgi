#!/bin/bash
#Copyright (C) 2014  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Acceptable Use"
TITLEHELP=$"The acceptable use policy gives new users a grace period to sign and return an acceptable use policy. User accounts are automatically disabled once the trial time is ended unless they are authorised."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Acceptable_Use"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

ICON1=/images/submenus/user/enabled.png
ICON2=/images/submenus/user/disabled.png

if [ "$MOBILE" = yes ]
then
	ICON1=/images/submenus/user/enabledm.png
	ICON2=/images/submenus/user/disabledm.png
fi


#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script>
<!--
function SetAllCheckBoxes(FormName, FieldName, CheckValue)
{
	if(!document.forms[FormName])
		return;
	var objCheckBoxes = document.forms[FormName].elements[FieldName];
	if(!objCheckBoxes)
		return;
	var countCheckBoxes = objCheckBoxes.length;
	if(!countCheckBoxes)
		objCheckBoxes.checked = CheckValue;
	else
		// set the check value for all check boxes
		for(var i = 0; i < countCheckBoxes; i++)
			objCheckBoxes[i].checked = CheckValue;
}
// -->
</script>
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function send_data {
Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/acceptable_use.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$GRACETIME:$USERNAMES:" | sudo -H /opt/karoshi/web_controls/exec/acceptable_use
}

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=21
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		if [ -z "$GET_TO_END" ]
		then
			DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		else
			DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER"-)
		fi
		break
	fi
	let COUNTER=$COUNTER+1
done
}


#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

[ -z "$ACTION" ] && ACTION=notset

if [ $ACTION = setgracetime ]
then

	#Assign GRACETIME
	DATANAME=GRACETIME
	get_data
	GRACETIME="$DATAENTRY"
	send_data
fi

if [ $ACTION = approve ]
then

	#Get users to approve
	DATANAME=USERNAME
	GET_TO_END="yes"
	get_data
	USERNAMES="${DATAENTRY//_ACTION_approve_USERNAME_/,}"
	GET_TO_END=""
fi

if [ "$ACTION" = enableac ] || [ "$ACTION" = disableac ]
then
	send_data
fi

#Check to see if acceptable use is enabled or disabled.
if [ -f /opt/karoshi/server_network/acceptable_use_authorisations/grace_time_disabled ]
then
	ACCEPTABLEUSESTATUS=$"Disabled"
	GRACETIMESTATUSMSG=$"Enable acceptable use"
	ICON="$ICON2"
	ACTION2=enableac
else
	ACCEPTABLEUSESTATUS=$"Enabled"
	GRACETIMESTATUSMSG=$"Disable acceptable use"
	ICON="$ICON1"
	ACTION2=disableac
fi

if [ "$ACTION" = resetstatus ]
then
	#Assign GROUP
	DATANAME=GROUP
	get_data
	GROUP="$DATAENTRY"

	Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/acceptable_use.cgi | cut -d' ' -f1)
	echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:::$GROUP:" | sudo -H /opt/karoshi/web_controls/exec/acceptable_use	
fi

[ -f /opt/karoshi/server_network/acceptable_use_authorisations/grace_time ] && GRACETIME=$(sed -n 1,1p /opt/karoshi/server_network/acceptable_use_authorisations/grace_time | tr -cd 0-9)
[ -z "$GRACETIME" ] && GRACETIME=14

if [ "$ACTION" = approve ]
then
	send_data
	#Reload page
	echo '<script>
	window.location = "acceptable_use.cgi";
	</script>'		
fi
#Show acceptable use options for admin staff.

echo '
<form action="/cgi-bin/admin/acceptable_use.cgi" name="acceptableuse" method="post">'




echo '
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Status"'</td>
				<td>
					<button class="info" name="_Status_" value="_ACTION_'"$ACTION2"'_">
						<img src="'"$ICON"'" alt="'"$GRACETIMESTATUSMSG"'">
						<span>'"$GRACETIMESTATUSMSG"'</span>
					</button>
				</td>
				<td><input name="_ACTION_'"$ACTION2"'_" type="submit" class="button" value="'"$ACCEPTABLEUSESTATUS"'"></td>
				<td></td>
			</tr>
			<tr>
				<td>'$"Grace Time"'</td>
				<td><input class="karoshi-input" required="required" maxlength="2" min="1" max="99" size="2" name="_GRACETIME_" value="'"$GRACETIME"'" type="number"></td>
				<td><input name="_ACTION_setgracetime_" type="submit" class="button" value="'$"Set Time"'"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Acceptable_Use"><span class="icon-large-tooltip">'$"The grace time is the amount of time a new user is allowed to log into the system before signing and returning the acceptable use policy. This time is set in days."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Reset Status"'</td>
				<td>'

#Show list of groups to reset the acceptable use grace time for
/opt/karoshi/web_controls/group_dropdown_list | sed 's%</select>%<option value="allusers">'$"All Users"'</option></select>%g'

echo '
				</td>
				<td><input name="_ACTION_resetstatus_" type="submit" class="button" value="'$"Reset"'"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Acceptable_Use"><span class="icon-large-tooltip">'$"Choose the group that you want to reset the acceptable use status for."'</span></a></td>
			</tr>
		</tbody>
	</table>'



#Get list of pending users
PROCESS_USERS=yes
if [ ! -d /opt/karoshi/server_network/acceptable_use_authorisations/pending ]
then
	PROCESS_USERS=no
fi

if [ "$PROCESS_USERS" = yes ]
then
	if [[ $(find /opt/karoshi/server_network/acceptable_use_authorisations/pending -maxdepth 1 -name '*' | sed '1d' | wc -l) -lt 1 ]]
	then
		PROCESS_USERS=no
	fi
fi

if [ "$PROCESS_USERS" = yes ]
then
	echo '<h2 style="display: inline;">'$"Pending Users"' </h2><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Acceptable_Use#View_pending_users"><span class="icon-large-tooltip">'$"The acceptable use policy gives new users a grace period to sign and return an acceptable use policy."' '$"User accounts are automatically disabled once the trial time is ended unless they are authorised."'</span></a><br><br>
	<table id="myTable" class="tablesorter" style="text-align: left;" ><thead>
	<tr><th style="width: 40%;"><b>'$"Username"'</b></th><th style="width: 40%;"><b>'$"Days Remaining"'</b></th><th style="width: 20%;"><b>'$"Approve"'</b></th></tr></thead><tbody>
	'
	for PENDING_USER_FULL in /opt/karoshi/server_network/acceptable_use_authorisations/pending/*
	do
		PENDING_USER=$(basename "$PENDING_USER_FULL")
		PENDING_USER_DATA=$(sed -n 1,1p "$PENDING_USER_FULL")
		DAY_COUNT=$(echo "$PENDING_USER_DATA" | cut -d, -f1)
		USER_CREATOR=$(echo "$PENDING_USER_DATA" | cut -d, -f2)
		CREATION_DATE=$(echo "$PENDING_USER_DATA" | cut -d, -f3)

		echo '<tr><td>'"$PENDING_USER"'</td><td>'"$DAY_COUNT"'</td><td><input id="Approve'"$PENDING_USER"'" name="_ACTION_approve_USERNAME_" value="'"$PENDING_USER"'" type="checkbox"><label for="Approve'"$PENDING_USER"'"></label></td></tr>'
	done

	echo '</tbody></table>'
fi


if [ "$PROCESS_USERS" = yes ]
then
	echo '<br><input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset"> <input class="button" type="button" onclick="SetAllCheckBoxes('\'acceptableuse\'', '\'_ACTION_approve_USERNAME_\'', true);" value="'$"Select all"'">'
fi

echo '</form></display-karoshicontent></body></html>'

exit
