#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Add Reverse Proxy"
TITLEHELP=$"The reverse proxy feature allows incoming web connections on port 80 and 443 to pass through the reverse proxy to other servers on your network."'<br><br>'$"This bypasses the need for sub domains and alias tcpip numbers for external access and will also allow all external ssl traffic to use one ssl certificate to authenticate the sites."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Reverse_Proxy_Server#Adding_Reverse_Proxy_Entries"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


#Get reverse proxy server
PROXYSERVER=$(sed -n 1,1p /opt/karoshi/server_network/reverseproxyserver | sed 's/ //g')

echo '

<form action="reverse_proxy_view_fm.cgi" method="post">
	<button class="button" name="_ViewReverseProxies_" value="_">
		'$"Reverse Proxies"'
	</button>
</form>
<form action="/cgi-bin/admin/reverse_proxy_add.cgi" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Target folder"'</td>
				<td><input class="karoshi-input" required="required" tabindex= "1" name="_TARGET_"type="text"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Reverse_Proxy_Server#Adding_Reverse_Proxy_Entries"><span class="icon-large-tooltip">'$"Enter in the web folder that you want redirected. Leave blank to re-direct the top directory."'<br><br>'$"Example: Joomla is installed at http://www.mysite/joomla"'<br><br>'$"Target folder - joomla"'<br><br>'$"Destination - http://www.mysite"'</span></a></td>
			</tr>
			<tr>
				<td>'$"Destination"'</td>
				<td><input class="karoshi-input" required="required" tabindex= "2" name="_DESTINATION_" type="text"></td>
				<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Reverse_Proxy_Server#Adding_Reverse_Proxy_Entries"><span class="icon-large-tooltip">'$"Enter in the web address that you want to redirect to."'<br><br>'$"Example: Joomla is installed at http://www.mysite/joomla"'<br><br>'$"Target folder - joomla"'<br><br>'$"Destination - http://www.mysite"'</span></a></td>
			</tr>
		</tbody>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>'
exit
