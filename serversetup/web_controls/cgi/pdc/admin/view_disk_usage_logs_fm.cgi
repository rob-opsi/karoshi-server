#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Disk Usage Logs"
TITLEHELP=$"The disk usage logs show the overall usage for each partition on your server."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Disk_Usage_Logs"

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
<script language="JavaScript" src="/all/calendar2/calendar_eu.js" type="text/javascript"></script>
<!-- Timestamp input popup (European Format) -->
<link rel="stylesheet" href="/all/calendar2/calendar.css">
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


#########################
#Get data input
#########################

DATE_INFO=$(date +%F)
DAY=$(echo "$DATE_INFO" | cut -d- -f3)
MONTH=$(echo "$DATE_INFO" | cut -d- -f2)
YEAR=$(echo "$DATE_INFO" | cut -d- -f1)

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

echo '
<form action="/cgi-bin/admin/view_disk_usage_logs.cgi" name="testform" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Log Date"'</td>
				<td>'
echo "
					<!-- calendar attaches to existing form element -->
					<input class=\"karoshi-input\" type=\"text\" value=\"$DAY-$MONTH-$YEAR\" size=14 maxlength=10 name=\"_DATE_\"></td>
				<td>
						<script type=\"text/javascript\">
						new tcal ({
							// form name
							'formname': 'testform',
							// input name
							'controlname': '_DATE_'
						});

						</script>
				</td>
			</tr>"

echo '
			<tr>
				<td>'$"View logs by date"'</td>
				<td></td>
				<td><input id="LogsToday" checked="checked" name="_LOGVIEW_" value="today" type="radio"><label for="LogsToday"> </label></td>
			</tr>
			<tr>
				<td>'$"View logs by month"'</td>
				<td></td>
				<td><input id="LogsMonth" name="_LOGVIEW_" value="month" type="radio"><label for="LogsMonth"> </label></td>
			</tr>
		</tbody>
	</table>
'

#Show list of servers
/opt/karoshi/web_controls/show_servers "$MOBILE" servers $"View disk logs"

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
