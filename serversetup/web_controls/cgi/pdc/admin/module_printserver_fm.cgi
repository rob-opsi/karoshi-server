#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')
#########################
#Assign data to variables
#########################
END_POINT=5
#Assign SERVERNAME

COUNTER=2
while [ "$COUNTER" -le "$END_POINT" ]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [ $(echo "$DATAHEADER"'check') = SERVERNAMEcheck ]
	then
		let COUNTER="$COUNTER"+1
		SERVERNAME=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER="$COUNTER"+1
done

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Setup Print Server"' - '"$SERVERNAME"
TITLEHELP=$"This will set up a print server for your network allowing printer queues to be created and controlled."' '$"Printer drivers can be added to the printer queues so that your client computers do not need to have the printer drivers installed on them."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Print_Server"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

function show_status {
echo '<SCRIPT language="Javascript">'
echo 'alert("'"$MESSAGE"'")';
echo 'window.location = "/cgi-bin/admin/karoshi_servers_view.cgi"'
echo '</script>'
echo "</body></html>"
exit
}

#########################
#Check data
#########################
#Check to see that servername is not blank
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"The servername cannot be blank."
	show_status
fi

#Check to see if this module has already been installed on the server
if [ -f /opt/karoshi/server_network/servers/"$SERVERNAME"/printserver ]
then
	STATUSMSG=$"This module has already been set up on this server."
fi

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"
echo '
			<form action="/cgi-bin/admin/module_printserver.cgi" method="post">
				<input name="_SERVERNAME_" value="'"$SERVERNAME"'" type="hidden">
				<b>'$"Description"'</b><br><br>
				'$"This will set up a print server for your network allowing printer queues to be created and controlled."' '$"Printer drivers can be added to the printer queues so that your client computers do not need to have the printer drivers installed on them."'<br><br>'"$STATUSMSG"'
				<br>
				<input value="'$"Submit"'" class="button primary" type="submit">
			</form>
		</display-karoshicontent>
	</body>
</html>
'
exit
