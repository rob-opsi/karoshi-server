#!/bin/bash
#Copyright (C) 2012  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Reset Room Controls"
TITLEHELP=$"This will schedule times when all internet room controls are reset to allow internet access."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Room_Controls"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-%')
#########################
#Assign data to variables
#########################
END_POINT=11
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

#Assign HOURS
DATANAME=HOURS
get_data
HOURS="$DATAENTRY"

#Assign MINUTES
DATANAME=MINUTES
get_data
MINUTES="$DATAENTRY"

#Assign TIME
DATANAME=TIME
get_data
TIME="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/dg_reset_room_controls_fm.cgi";
</script>
</body>
</html>'
exit
}

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that ACTION is not blank
if [ -z "$ACTION" ]
then
	MESSAGE=$"You have not entered an action."
	show_status
fi
if [ "$ACTION" = add ]
then
	#Check to see that HOURS are not blank
	if [ -z "$HOURS" ]
	then
		MESSAGE=$"You have not entered a time."
		show_status
	fi
	#Check to see that MINUTES are not blank
	if [ -z "$MINUTES" ]
	then
		MESSAGE=$"You have not entered a time."
		show_status
	fi
fi
if [ "$ACTION" = delete ]
then
	#Check to see that TIME is not blank
	if [ -z "TIME" ]
	then
		MESSAGE=$"You have not entered a time."
		show_status
	fi
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/dg_reset_room_controls.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$HOURS:$MINUTES:$TIME:" | sudo -H /opt/karoshi/web_controls/exec/dg_reset_room_controls
echo '
<script>
	window.location = "/cgi-bin/admin/dg_reset_room_controls_fm.cgi";
</script>
</body>
</html>'
exit
