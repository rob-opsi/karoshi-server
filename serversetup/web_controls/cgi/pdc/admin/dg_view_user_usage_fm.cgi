#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"User Internet Usage"
TITLEHELP=$"This shows the number of sites a user has visited."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/script.js"></script>'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
			<form action="/cgi-bin/admin/dg_view_user_usage.cgi" name="testform" method="post">
				<table>
					<tbody>
						<tr>
							<td style="width: 180px;">'$"Username"'</td>
							<td><div id="suggestions"></div><input required="required" tabindex= "1" name="_USERNAME_" class="karoshi-input" size="14" type="text" id="inputString" onkeyup="lookup(this.value);"></td>
							<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the username that you want to check the internet logs for."'</span></a></td>
						</tr>
					</tbody>
				</table>
				<input value="'$"Submit"'" class="button primary" type="submit">
			</form>
		</display-karoshicontent>
	</body>
</html>
'
exit

