#!/bin/bash
#Copyright (C) 2010  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Windows Commands"
TITLEHELP=$"This will send commands to a windows machine joined to your network."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Redirect to windows_servers_add_fm.cgi if no windows servers have been added
if [ ! -d /opt/karoshi/server_network/windows_servers/ ]
then
	echo '<form action="windows_servers_add_fm.cgi" method="post">/form>
	<SCRIPT LANGUAGE="JavaScript">document.forms[1].submit();</SCRIPT></body></html>'
fi

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

echo '
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Command"'</td>
				<td>
				        <select name="_COMMAND_" class="karoshi-input">
						<option></option>
						<option value="shutdown">'$"Shutdown"'</option>
						<option value="restart">'$"Restart"'</option>
						<option value="abortshutdown">'$"Abort shutdown"'</option>
						<option value="startservice">'$"Start service"'</option>
						<option value="stopservice">'$"Stop service"'</option>
						<option value="servicestatus">'$"Service status"'</option>'
					#	<option value="showprinters">'$"Show printers"'</option>
echo	'
						<option value="showshares">'$"Show shares"'</option>
						<option value="showfiles">'$"Show open files"'</option>
					</select>
				</td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Choose the command that you want to be carried out."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Extra options"'</td>
				<td><input tabindex= "5" name="_OPTIONS_" class="karoshi-input" size="20" type="text"></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"If a command needs extra options you can add them here."'</span></a></td>
			</tr>
		</tbody>
	</table>
<br>'

#Show list of enabled servers
SERVERLISTARRAY=( `ls -1 /opt/karoshi/server_network/windows_servers/` )
SERVERLISTCOUNT=${#SERVERLISTARRAY[@]}
SERVERCOUNTER=0
SERVERICON="/images/submenus/system/computer.png"
SERVERICON2="/images/submenus/system/all_computers.png"
echo '<table class="standard" style="text-align: left;" ><tbody><tr>'
while [ "$SERVERCOUNTER" -lt "$SERVERLISTCOUNT" ]
do
	KAROSHISERVER="${SERVERLISTARRAY[$SERVERCOUNTER]}"
	echo '<td style="width: 90px; vertical-align: top; text-align: left;">
	<button class="info" name="_Server_" value="_SERVER_'"$KAROSHISERVER"'_">
	<img src="'$SERVERICON'" alt="'"$KAROSHISERVER"'">
	<span>'"$KAROSHISERVER"'</span>
	</button>
	<br>'"$KAROSHISERVER"'</td>'
	[ "$SERVERCOUNTER" = 5 ] && echo '</tr><tr>'
	let SERVERCOUNTER="$SERVERCOUNTER"+1
done
echo '</tr><tr><td style="width: 90px; vertical-align: top; text-align: left;">

<button class="info" name="_Server_" value="_SERVER_allservers_">
<img src="'$SERVERICON2'" alt="'$"All Servers"'">
<span>'$"All Servers"'</span>
</button>
<br>'$"All Servers"'</td>'
echo '</tr></tbody></table></form></display-karoshicontent></body></html>'

exit
