#!/bin/bash
#Copyright (C) 2014  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk
#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Ban User Internet Access"
TITLEHELP=$"This page allows you to ban or allow user internet access."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=User_Internet_Access"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '
<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-+%')
#########################
#Assign data to variables
#########################
END_POINT=10
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign ACTION
DATANAME=ACTION
get_data
ACTION="$DATAENTRY"

if [ ! -z "$ACTION" ]
then
	#Assign USERNAMES
	DATANAME=USERNAMES
	get_data
	USERNAMES="${DATAENTRY//%40/@}"

	if [ "$ACTION" = banusers ]
	then
		END_POINT=22
		#Assign HOUR
		DATANAME=HOUR
		get_data
		HOUR="$DATAENTRY"

		#Assign MINUTES
		DATANAME=MINUTES
		get_data
		MINUTES="$DATAENTRY"

		#Assign DAY
		DATANAME=DAY
		get_data
		DAY="$DATAENTRY"

		#Assign MONTH
		DATANAME=MONTH
		get_data
		MONTH="$DATAENTRY"

		#Assign YEAR
		DATANAME=YEAR
		get_data
		YEAR="$DATAENTRY"

		#Assign INCIDENT
		DATANAME=INCIDENT
		get_data
		INCIDENT="$DATAENTRY"

		#Assign ACTIONTAKEN
		DATANAME=ACTIONTAKEN
		get_data
		ACTIONTAKEN="$DATAENTRY"

		#Assign BANLENGTH
		DATANAME=BANLENGTH
		get_data
		BANLENGTH="$DATAENTRY"
	fi
fi

[ -z "$ACTION" ] && ACTION=view 

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/user_internet_access.cgi | cut -d' ' -f1)
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$ACTION:$USERNAMES:$HOUR:$MINUTES:$DAY:$MONTH:$YEAR:$BANLENGTH:$INCIDENT:$ACTIONTAKEN:$MOBILE:" | sudo -H /opt/karoshi/web_controls/exec/user_internet_access

[ "$MOBILE" = no ] && echo '</div>'

echo '
</display-karoshicontent>
</body>
</html>'
exit

