#!/bin/bash
#Copyright (C) 2014  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Create Commercial Certificate"
TITLEHELP=$"This is a three stage process to create and install a commercial SSL certificate for your domain."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Commercial_SSL_Certificate"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=8
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign SERVER
DATANAME=SERVER
get_data
SERVER="$DATAENTRY"

[ -z "$SERVER" ] && SERVER=notset


############################
#Stage 1 - Create server key and server.crt
############################

echo '
<form action="/cgi-bin/admin/ssl_commercial_certs.cgi" name="selectservers" method="post">'

#Show step 1

echo '
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Step"' 1</td>
			<td><input name="___SERVERNAME___'"$REALM"'___SERVERTYPE___network___SERVERMASTER___notset___ACTION___getcertdetails___" type="submit" class="button" value="'$"Create Certificate"'"></td>
		</tr>'

if [ -f /opt/karoshi/server_network/ssl/commercial_ssl_certs/"$REALM" ]
then
	echo '
		<tr>
			<td>'$"Step"' 2</td>
			<td><input name="___SERVERNAME___'$REALM'___SERVERTYPE___network___SERVERMASTER___notset___ACTION___copycertinfo___" type="submit" class="button" value="'$"Copy Certificate"'"></td>
		</tr>
		<tr>
			<td>'$"Step"' 3</td>
			<td>'$"Install certificate"'</td>
		</tr>'
fi

echo '
	</tbody>
</table>
<br>
'

if [ -f /opt/karoshi/server_network/ssl/commercial_ssl_certs/"$REALM" ]
then
	#Show list of servers
	/opt/karoshi/web_controls/show_servers "$MOBILE" servers $"Install certificate" getinstallcertinfo no ___
fi

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
