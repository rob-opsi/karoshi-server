#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Server Information"

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser
source /opt/karoshi/web_controls/version

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/server_info.cgi" name="selectservers" method="post">
	<table>
		<tbody>
			<tr>
				<td style="width: '$WIDTH'px;">'$"Disk drives"'</td>
				<td><input id="HardDrive" name="_INFO_" value="harddrive" checked="checked" type="radio"><label for="HardDrive"> </label></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Show information about the hard disk drives on the selected servers."'</span></a></td>
			</tr>
			<tr>
				<td>'$"CPU usage"'</td>
				<td><input id="CPUUsage" name="_INFO_" value="cpu" type="radio"><label for="CPUUsage"> </label></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Show cpu information on the selected servers."'</span></a></td>
			</tr>
			<tr>
				<td>'$"Kernel"'</td>
				<td><input id="KernelInfo" name="_INFO_" value="kernel" type="radio"><label for="KernelInfo"> </label></td>
				<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Show the running kernel for the selected servers."'</span></a></td>
			</tr>
		</tbody>
	</table><br><br>'

#Show list of servers
/opt/karoshi/web_controls/show_servers $MOBILE all $"Show server info"


echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
