#!/bin/bash
#Copyright (C) 2015  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Dynamic Groups"
TITLEHELP=$"This allows you to create groups of users that change on a regular basis."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Group_Management"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
	<form enctype="multipart/form-data" ACTION="/cgi-bin/admin/dynamic_groups_upload.cgi" METHOD="POST">
		<button class="buttton" formaction="groups.cgi" name="_ViewNewPasswords_" value="_">
			'$"Group Management"'
		</button>
		<br><br>
		<table>
			<tr>
				<td style="width: 180px;">'$"Upload CSV file"'</td>
				<td><INPUT TYPE="FILE" NAME="file-to-upload-01" SIZE="35"> <a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Group_Management"><span class="icon-large-tooltip">'$"CSV Format""<br>"$"username or enrolment number or staff code,group1,group2,group3..."'</span></a>
				</td>
			</tr>
		</table>
		<br>
		  <input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
	</form>
	</display-karoshicontent>
</body>
</html>
'
exit
