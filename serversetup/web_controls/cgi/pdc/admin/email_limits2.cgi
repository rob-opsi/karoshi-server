#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"E-Mail Limits"
TITLEHELP=$"E-Mail size and queue limits."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=E-Mail_Limits"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=8
#Assign MESSAGESIZE
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = MESSAGESIZEcheck ]
	then
		let COUNTER=$COUNTER+1
		MESSAGESIZE=`echo $DATA | cut -s -d'_' -f$COUNTER | tr -cd '0-9\n'`
		break
	fi
	let COUNTER=$COUNTER+1
done
#Assign MAILBOXSIZE
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = MAILBOXSIZEcheck ]
	then
		let COUNTER=$COUNTER+1
		MAILBOXSIZE=`echo $DATA | cut -s -d'_' -f$COUNTER | tr -cd '0-9\n'`
		break
	fi
	let COUNTER=$COUNTER+1
done
#Assign MAILWARN
COUNTER=2
while [ $COUNTER -le $END_POINT ]
do
	DATAHEADER=`echo $DATA | cut -s -d'_' -f$COUNTER`
	if [ `echo $DATAHEADER'check'` = MAILWARNcheck ]
	then
		let COUNTER=$COUNTER+1
		MAILWARN=`echo $DATA | cut -s -d'_' -f$COUNTER | tr -cd '0-9\n'`
		break
	fi
	let COUNTER=$COUNTER+1
done

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_$HTTPS != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ $REMOTE_USER'null' = null ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [ `grep -c ^$REMOTE_USER: /opt/karoshi/web_controls/web_access_admin` != 1 ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################
#Check to see that MESSAGESIZE is not blank
if [ -z "$MESSAGESIZE" ]
then
	MESSAGE=$"The message size option must not be blank."
	show_status
fi
#Check to see that MAILBOXSIZE is not blank
if [ -z "$MAILBOXSIZE" ]
then
	MESSAGE=$"The mailbox size option must not be blank."
	show_status
fi

#Check to see that MAILWARN is not blank
if [ -z "$MAILBOXSIZE" ]
then
	MESSAGE=$"The mailwarn size option must not be blank."
	show_status
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/email_limits2.cgi | cut -d' ' -f1)
#Make changes
sudo -H /opt/karoshi/web_controls/exec/email_limits "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$MESSAGESIZE:$MAILBOXSIZE:$MAILWARN"
echo '
<script>
	window.location = "/cgi-bin/admin/email_limits.cgi"
</script>
'
exit
