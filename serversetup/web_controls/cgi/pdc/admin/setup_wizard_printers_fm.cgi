#!/bin/bash
#Copyright (C) 2011  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Setup Printers"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<br>
'$"If you have not already done so you may want to add a printer server to your network so that you can control printing for your clients."'
<br>
<br>
'$"The show servers link below will show all available servers. Choose the server that you want to add the printer module to by clicking on the add role button."'
<br>
<br>
<br>

<table>
	<tbody>
		<tr>
			<td style="width: 30px;"><a href="karoshi_servers_view.cgi"><img src="/images/submenus/system/computer.png" border="0"></a></td>
			<td><a href="karoshi_servers_view.cgi">'$"Show Servers"'</a></td>
		</tr>
		<tr>
			<td><a href="printers_add_fm.cgi"><img src="/images/submenus/printer/add_printer.png" border="0"></a></td>
			<td><a href="printers_add_fm.cgi">'$"Add Printer"'</a></td>
		</tr>
		<tr>
			<td><a href="printers.cgi"><img src="/images/submenus/printer/view_print_queues.png" border="0"></a></td>
			<td><a href="printers.cgi">'$"Control Printers"'</a></td>
		</tr>
	</tbody>
</table>
</display-karoshicontent>
</body>
</html>
'
exit

