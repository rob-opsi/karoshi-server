#!/bin/bash
#Copyright (C) 2015 Matthew Jowett
#
#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Setup Gitlab"
TITLEHELP=$"This will setup a Gitlab Server (CE 13.3 Omnibus)."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Gitlab_Server"

#########################
#Get data input
#########################

DATA=$(cat | tr -cd 'A-Za-z0-9\._:%\-+')
#########################
#Assign data to variables
#########################
END_POINT=5
#Assign SERVERNAME

COUNTER=2
while [ "$COUNTER" -le "$END_POINT" ]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [ $(echo "$DATAHEADER"'check') = SERVERNAMEcheck ]
	then
		let COUNTER=$COUNTER+1
		SERVERNAME=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER="$COUNTER"+1
done

[ ! -z  "$SERVERNAME" ] && TITLE="$TITLE - $SERVERNAME"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/karoshi_servers_view.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check data
#########################
#Check to see that servername is not blank
if [ -z "$SERVERNAME" ]
then
	MESSAGE=$"The server cannot be blank."
	show_status
fi

echo '<form  id="form1" name="combobox" action="/cgi-bin/admin/module_gitlab.cgi" method="post">

<input name="_SERVERNAME_" value="'"$SERVERNAME"'" type="hidden">
<b>'$"Description"'</b><br>
<br>This will setup a Gitlab Server (CE 13.3 Omnibus).<br>
<a class="info" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Gitlab_Server"><br></a>

<br><b>'$"Parameters"'</b><br><br>
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Gitlab Domain"'</td>
			<td class="karoshi-input"><input type="text" name="_ALIAS_" class="karoshi-input" value="" size="10"></td>
			<td>.'"$REALM"'</td>
			<td><a class="info icon icon-large fa-info-circle" target="_blank" href="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Gitlab_Server"><span class="icon-large-tooltip">'$"You will need to choose an alias for this server for web access. Either enter in a custom alias or choose one from the dropdown list."'</span></a></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<select name="_ALIASLIST_" class="karoshi-input" size="1" onchange="document.combobox._ALIAS_.value = document.combobox._ALIASLIST_.options[document.combobox._ALIASLIST_.selectedIndex].value;document.combobox._ALIASLIST_.value=&#39;&#39;">
					<option label=" " value="" selected="selected"></option>'

#Show alias choice

if [ -f /opt/karoshi/server_network/aliases/"$SERVERNAME" ]
then
	#Show any custom aliases that have been assigned
	echo '
					<option style="color:black ; font-weight:bold" value="">'$"Assigned Aliases"'</option>'
	for CUSTOM_ALIAS in $(cat /opt/karoshi/server_network/aliases/"$SERVERNAME")
	do
		echo '
					<option style="color:green">'"$CUSTOM_ALIAS"'</option>'
	done
	echo '
					<option style="color:black ; font-weight:bold" value="">'$"Unassigned Aliases"'</option>'
fi

#Get a set of available aliases to check

#Check www.realm
[[ $(nslookup www.$REALM 127.0.0.1 | grep -c ^Name:) = 0 ]] && echo '<option>www</option>'
COUNTER=1
while [ "$COUNTER" -le 10 ]
do
	[ $(nslookup www"$COUNTER.$REALM" 127.0.0.1 | grep -c ^Name:) = 0 ] && echo '<option>www'"$COUNTER"'</option>'
	let COUNTER="$COUNTER"+1
done
echo '
				</select></td>
			<td></td>
			<td></td>
		</tr>
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit">
</form>
</display-karoshicontent>
</body>
</html>'
exit

########################
#Random Key
#R0ZM927copgc5COOvluL+O/mmd7jZWLy9sfG9VYYJSE=
########################
