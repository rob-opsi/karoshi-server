#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

#Detect mobile browser

source /opt/karoshi/web_controls/detect_mobile_browser

TITLE=$"View Network Backup Logs"
TITLEHELP=$"Choose the server that you want to view the backup logs for."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=View_Backup_Logs"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'
#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

DATE_INFO=$(date +%F -d "yesterday")
DAY=$(echo "$DATE_INFO" | cut -d- -f3)
MONTH=$(echo "$DATE_INFO" | cut -d- -f2)
YEAR=$(echo "$DATE_INFO" | cut -d- -f1)

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/karoshi_servers_view.cgi"
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#Check to see that a backup server has been configured
if [ ! -d /opt/karoshi/server_network/backup_servers/backup_settings ]
then
	MESSAGE=$"No karoshi backup servers have been enabled for ssh."
	show_status
fi

if [[ $(ls -1 /opt/karoshi/server_network/backup_servers/backup_settings | wc -l) = 0 ]]
then
	MESSAGE=$"No karoshi backup servers have been enabled for ssh."
	show_status
fi

echo '
<form action="/cgi-bin/admin/backup_view_logs.cgi" method="post">
<table>
	<tbody>
		<tr>
			<td class="karoshi-input"><b>'$"Log Date"'</b></td>
			<td>'
echo "
				<!-- calendar attaches to existing form element -->
				<input class=\"karoshi-input\" required=\"required\" type=\"text\" value=\"$DAY-$MONTH-$YEAR\" size=14 maxsize=10 name=\"_DATE_\" /></td><td style=\"vertical-align: top; text-align: center;\">
				<script language=\"JavaScript\">
					new tcal ({
						// form name
						'formname': 'testform',
						// input name
						'controlname': '_DATE_'
					});

				</script>
			</td>
		</tr>
	</tbody>
</table>"

#Show list of servers
MOBILE=no
/opt/karoshi/web_controls/show_servers "$MOBILE" backups $"Show logs"

echo '
</form>
</display-karoshicontent>
</body>
</html>'
exit
