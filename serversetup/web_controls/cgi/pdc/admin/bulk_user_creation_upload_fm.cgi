#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Bulk User Creation"
TITLEHELP=$"Please check the information on this page and the online help for the csv file format."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Upload_CSV"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Check password settings
source /opt/karoshi/server_network/security/password_settings

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<FORM ENCTYPE="multipart/form-data" ACTION="/cgi-bin/admin/bulk_user_creation_upload.cgi" METHOD="POST">
	<button class="button" formaction="bulk_user_creation_view_passwords_fm.cgi" name="_ViewNewPasswords_" value="_">
		'$"View New Passwords"'
	</button>

	<button class="button" formaction="user_image_upload_fm.cgi" name="_ImportUserImages_" value="_">
		'$"Import User Images"'
	</button>

	<button class="button" formaction="bulk_user_creation_import_enrollment_numbers_fm.cgi" name="_ImportEnrollmentNumbers_" value="_">
		'$"Import Enrollment Numbers"'
	</button>

	<button class="button" formaction="csv_set_passwords_fm.cgi" name="_SetUserPasswords_" value="_">
		'$"Set User Passwords"'
	</button>
	<br>
	<br>
	<b>'$"CSV file format without the first row as headers"'</b><br>'$"Forename, surname, enrolment number or staff code - optional, username - optional, primary group - optional, secondary groups separated by colons - optional, change password on first logon - optional (y/n), room-number - optional, telephone-number - optional, fax-number - optional, mobile-number - optional, password - optional"''
	[ "$PASSWORDCOMPLEXITY" = on ] && echo '. '$"Upper and lower case characters and numbers are required."''
echo '
	<br><br><b>'$"Example"'</b><br>John,Jones,16-089,,,letme-in<br><br>

	<b>'$"CSV file format with the first row as headers in any order"'</b><br>
	'$"Possible column headers are"' forename, surname, enrolment-number, username, primary-group, secondary-groups, change-password-on-logon, room-number, telephone-number, fax-number, mobile-number,password
	<br>
	<br>
	<b>'$"Example"'</b><br>
	surname,forename,password,enrolment-number<br>
	Jones,John,hello123,JJ1<br>
	Wilson,Mark,foobar123,MW1<br>
	May,Amy,marmaduke,AM1<br><br>
	

	<table>
		<tr>
			<td style="width: 180px;">'$"Upload CSV file"'</td>
			<td><INPUT TYPE="FILE"  NAME="file-to-upload-01" SIZE="35"></td>
		</tr>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit">
</form>
</display-karoshicontent>
</body>
</html>
'
exit
