#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Disk Usage Logs"
TITLEHELP=$"The disk usage logs show the overall usage for each partition on your server."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Disk_Usage_Logs"

#Detect mobile browser
MOBILE=no
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

############################
#Show page
############################
function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/view_disk_usage_logs_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}
#########################
#Get data input
#########################

DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')

#If we have not received any data via post then try and get it from query_string
if [ -z "$DATA" ]
then
	DATA=$(echo "$QUERY_STRING" | tr -cd 'A-Za-z0-9\_.-/-' | sed 's/\.\.\///g')
fi
#########################
#Assign data to variables
#########################
END_POINT=38
function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}

#Assign DATE
DATANAME=DATE
get_data
DATE="$DATAENTRY"

#Assign LOGVIEW
DATANAME=LOGVIEW
get_data
LOGVIEW="$DATAENTRY"

#Assign SERVERNAME
DATANAME=SERVERNAME
get_data
SERVER="$DATAENTRY"

#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi
#########################
#Check data
#########################

#Check to see that LOGVIEW is not blank
if [ -z "$LOGVIEW" ]
then
	MESSAGE=$"The logview cannot be blank."
	show_status
fi
#Check to see that DATE is not blank
if [ -z "$DATE" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi


#Check that date is valid
DAY=$(echo "$DATE" | cut -d- -f1)
MONTH=$(echo "$DATE" | cut -d- -f2)
YEAR=$(echo "$DATE" | cut -d- -f3)

#Check to see that MONTH is not blank
if [ -z "$MONTH" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi
#Check to see that YEAR is not blank
if [ -z "$YEAR" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi

if [ "$DAY" -gt 31 ]
then
	MESSAGE=$"Incorrect date format."
	show_status
fi

if [ "$MONTH" -gt 12 ]
then
	MESSAGE=$"Incorrect date format."
	show_status
fi

if [ "$YEAR" -lt 2006 ] || [ "$YEAR" -gt 3006 ]
then
	MESSAGE=$"Incorrect date format."
	show_status
fi

#Check to see that server is not blank
if [ -z "$SERVER" ]
then
	MESSAGE=$"You have not chosen any servers."
	show_status
fi


echo '
<form action="view_disk_usage_logs_fm.cgi" name="selectservers" method="post">
	<button class="button" name="SelectServer" value="__">
		'$"Select Server"'
	</button>
</form>
'

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/admin/view_disk_usage_logs.cgi | cut -d' ' -f1)
sudo -H /opt/karoshi/web_controls/exec/view_disk_usage_logs "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$LOGVIEW:$DAY:$MONTH:$YEAR:$SERVER:$MOBILE:"
echo '</display-karoshicontent></body></html>'
exit
