#!/bin/bash
#copy_files_upload
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Windows Desktop Icons"
TITLEHELP=$"Choose the icons that you want to upload."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Upload_Desktop_Icons"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

echo '
		<P>
		'$"Select the icons that you want to upload."'
		<P>
		<FORM ENCTYPE="multipart/form-data" ACTION="/cgi-bin/admin/windows_client_icon_upload.cgi" METHOD="POST">
			<table>
				<tr>
					<td class="karoshi-input" ALIGN=left>'$"Icon"' 1:</td>
					<td><INPUT TYPE="FILE" NAME="file-to-upload-01" SIZE="35"></td>
				</tr>
				<tr>
					<td>'$"Icon"' 2:</td>
					<td><INPUT TYPE="FILE" NAME="file-to-upload-02" SIZE="35"></td>
				</tr>
				<tr>
					<td>'$"Icon"' 3:</td>
					<td><INPUT TYPE="FILE" NAME="file-to-upload-03" SIZE="35"></td>
				</tr>
				<tr>
					<td>'$"Icon"' 4:</td>
					<td><INPUT TYPE="FILE" NAME="file-to-upload-04" SIZE="35"></td>
				</tr>
				<tr>
					<td>'$"Icon"' 5:</td>
					<td><INPUT TYPE="FILE" NAME="file-to-upload-05" SIZE="35"></td>
				</tr>
				<tr>
					<td>'$"Icon"' 6:</td>
					<td><INPUT TYPE="FILE" NAME="file-to-upload-06" SIZE="35"></td>
				<tr>
					<td>'$"Icon"' 7:</td>
					<td><INPUT TYPE="FILE" NAME="file-to-upload-07" SIZE="35"></td>
				</tr>
				<tr>
					<td>'$"Icon"' 8:</td>
					<td><INPUT TYPE="FILE" NAME="file-to-upload-08" SIZE="35"></td>
				</tr>
			</table>
			<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
		</form>
		</display-karoshicontent>
	</body>
</html>
'
exit
