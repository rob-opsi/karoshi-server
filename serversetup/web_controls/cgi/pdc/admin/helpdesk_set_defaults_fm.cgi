#!/bin/bash
#Copyright (C) 2012  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Technical Support Set Defaults"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/admin/helpdesk_set_defaults.cgi" method="post">
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Default assign jobs"'</td>
			<td>
				<select class="karoshi-input" tabindex= "1" class="karoshi-input" name="_DEFAULTNAME_">'

if [ -f /opt/karoshi/server_network/helpdesk/defaultassign ]
then
	ASSIGNED=$(sed -n 1,1p /opt/karoshi/server_network/helpdesk/defaultassign)
	echo '
					<option value="'"$ASSIGNED"'">'"$ASSIGNED"'</option>'
else
	ASSIGNED=no
fi
echo '
					<option value="NODEFAULTNAME">'$"No default"'</option>'
for ADMINUSER in $(cat /opt/karoshi/web_controls/web_access_admin | cut -d: -f1)
do
	[ $ADMINUSER != $ASSIGNED ] && echo '
					<option value="'$ADMINUSER'">'$ADMINUSER'</option>'
done
for TECHUSER in `cat /opt/karoshi/web_controls/web_access_tech | cut -d: -f1`
do
	[ "$TECHUSER" != "$ASSIGNED" ] && echo '
					<option value="'"$TECHUSER"'">'"$TECHUSER"'</option>'
done
echo '
				</select>
			</td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Choose the administrator or technician that you want helpdesk tasks to default to."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Default priority"'</td>
			<td>
				<select class="karoshi-input" tabindex= "2" class="karoshi-input" name="_DEFAULTPRIORITY_">'

if [ -f /opt/karoshi/server_network/helpdesk/defaultpriority ]
then
	PRIORITY=$(sed -n 1,1p /opt/karoshi/server_network/helpdesk/defaultpriority)
	echo '
					<option value="'"$PRIORITY"'">'"$PRIORITY"'</option>'
fi
echo '
					<option value="NODEFAULTPRIORITY">'$"No default"'</option>'
[ "$PRIORITY" != $"Urgent" ] && echo '
					<option value="'$"Urgent"'">'$"Urgent"'</option>'
[ "$PRIORITY" != $"High" ] && echo '
					<option value="'$"High"'">'$"High"'</option>'
[ "$PRIORITY" != $"Medium" ] && echo '
					<option value="'$"Medium"'">'$"Medium"'</option>'
[ "$PRIORITY" != $"Urgent" ] && echo '
					<option value="'$"Low"'">'$"Low"'</option>'

echo '
				</select>
			</td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Choose the default priority that you want."'</span></a></td>
		</tr>
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>
'
exit

