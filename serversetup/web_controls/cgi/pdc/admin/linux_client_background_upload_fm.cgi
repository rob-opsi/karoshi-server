#!/bin/bash
#Copyright (C) 2007  Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Linux Background - Upload"
TITLEHELP=$"This will replace the standard background with one of your choice. The backgrounds are applied when the client computer is rebooted."
HELPURL="http://www.linuxschools.com/karoshi/documentation/wiki/index.php?title=Linux_Client_Background"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
	<display-karoshicontent>
	<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"	

echo '
	<form action="/cgi-bin/admin/linux_client_choose_background_fm.cgi" method="post">
		<button class="button" name="_VIEW_" value="_">
			'$"View Backgrounds"'
		</button>
	</form>
	<p>'$"Select the background that you want to upload."'<p>
	<form enctype="multipart/form-data" ACTION="/cgi-bin/admin/linux_client_background_upload.cgi" METHOD="POST">
		<table>
			<tr>
				<td class="karoshi-input">'$"Background"'</td>
				<td><INPUT accept=".png" TYPE="FILE" NAME="file-to-upload-01" SIZE="30"></td>
				<td style="vertical-align: middle;"><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"CHoose the png format image that you want to upload."'</span></a></td>
			</tr>
		</table>
		<input value="'$"Submit"'" class="button primary" type="submit">
	</form>
	</display-karoshicontent>
</body>
</html>'
exit
