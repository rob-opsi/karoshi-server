#!/bin/bash
#Copyright (C) 2007 Paul Sharrad

#This file is part of Karoshi Server.
#
#Karoshi Server is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Karoshi Server is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with Karoshi Server.  If not, see <http://www.gnu.org/licenses/>.

#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Wake a location"
TITLEHELP=$"This will schedule all of the computers in your selected location to be turned on."
HELPURL=$"This will turn on all of the computers in your selected location."

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/admin/locations.cgi";

</script>
</body>
</html>'
exit
}
#########################
#Check https access
#########################
if [ https_"$HTTPS" != https_on ]
then
	export MESSAGE=$"You must access this page via https."
	show_status
fi
#########################
#Check user accessing this script
#########################
if [ ! -f /opt/karoshi/web_controls/web_access_admin ] || [ -z "$REMOTE_USER" ]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

if [[ $(grep -c ^"$REMOTE_USER:" /opt/karoshi/web_controls/web_access_admin) != 1 ]]
then
	MESSAGE=$"You must be a Karoshi Management User to complete this action."
	show_status
fi

echo '
<form action="/cgi-bin/admin/wake_on_lan_now2.cgi" method="post">

	<button formaction="wake_on_lan_add.cgi" class="button" name="_ScheduleWakeonLan_" value="_">
		'$"Schedule Location"'
	</button>

	<button formaction="wake_on_lan_view.cgi" class="button" name="_ScheduleWakeonLan_" value="_">
		'$"Scheduled Locations"'
	</button>
</form>
<form action="/cgi-bin/admin/wake_on_lan_now2.cgi" method="post">
'
#Time to wake location up
echo '
<table>
	<tbody>
		<tr>
			<td class="karoshi-input"><b>'$"Location"'</b></td>
			<td>'
#Show current rooms
LOCATION_COUNT=$(wc -l < /var/lib/samba/netlogon/locations.txt)
if [ "$LOCATION_COUNT" -gt 0 ]
then
	echo '
				<select class="karoshi-input" name="_LOCATION_">'
	COUNTER=1
	while [ "$COUNTER" -le "$LOCATION_COUNT" ]
	do
		LOCATION=$(sed -n "$COUNTER,$COUNTER"'p' /var/lib/samba/netlogon/locations.txt)
		echo '
					<option value="'"$LOCATION"'">'"$LOCATION"'</option>'
		let COUNTER="$COUNTER"+1
	done
	echo '
				</select>
			</td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Select the location that you want. All computers in this location will be turned on."'</span></a></td>
		</tr>
	</tbody>
</table>'
else
	MESSAGE=$"There are no locations to schedule."
	show_status
fi

echo '
<input class="button primary" value="Submit" type="submit"> <input value="Reset" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>'
exit
