#!/bin/bash
#Copyright (C) 2011  Paul Sharrad
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/get_user_prefs

TITLE=$"Set Default Page"
TITLEHELP=$"Choose the default page that you want to have for this section of the web management."
HELPURL="#"

#Generate page layout
source /opt/karoshi/web_controls/generate_page_admin

#Append Karoshi content to the #karoshicontent container.
echo "
<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"


echo '
<form action="/cgi-bin/tech/set_default_page.cgi" method="post">
	<table>
		<tbody>
			<tr>
				<td class="karoshi-input">'$"Default Page"'</td>
				<td>
					<select class="karoshi-input" required="required" name="_DEFAULTPAGE_">
						<option label="blank"></option>
						<option value="add_user_fm.cgi">'$"Add Users"'</option>
						<option value="change_password_fm.cgi">'$"Change User Passwords"'</option>
						<option value="show_user_info_fm.cgi">'$"Show User Information"'</option>
						<option value="lockout_reset_fm.cgi">'$"Reset User Lockout"'</option>
						<option value="incident_log_view_fm.cgi">'$"View Incident Logs"'</option>
						<option value="helpdesk_view_fm.cgi">'$"Help Desk Requests"'</option>'

[ -f /opt/karoshi/server_network/printserver ] && echo '
						<option value="printers.cgi">'$"Printer Queues"'</option>'
[ -f /opt/karoshi/server_network/monitoring_server ] &&	echo '
						<option value="mon_status.cgi">'$"System Monitoring"'</option>'

echo '
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>
'
exit
