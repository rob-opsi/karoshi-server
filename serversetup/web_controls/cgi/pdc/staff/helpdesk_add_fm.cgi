#!/bin/bash
#Copyright (C) 2010  Paul Sharrad
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/global_prefs

TITLE=$"Technical Support"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_staff

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

echo '
<form action="/cgi-bin/staff/helpdesk_add.cgi" method="post">
<table>
	<tbody>
		<tr>
			<td class="karoshi-input">'$"Request Summary"'</td>
			<td><input class="karoshi-input" tabindex= "1" maxlength="24" size="20" name="_JOBTITLE_" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in a title or summary for the job that you want completed."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Computer Number"'</td>
			<td><input class="karoshi-input" tabindex= "3" maxlength="10" size="20" name="_ASSETNUMBER_" type="text"></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"The computer number is used to help identify where it is situated in the room. This can be left blank."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Location"'</td>
			<td>
				<select class="karoshi-input" tabindex= "2" name="_LOCATION_">
					<option label="blank" value=""></option>
'

###############################
#Location
###############################
if [ -f /var/lib/samba/netlogon/locations.txt ]
then
	LOCATION_COUNT=$(cat /var/lib/samba/netlogon/locations.txt | wc -l)
else
	LOCATION_COUNT=0
fi

COUNTER=1
while [ "$COUNTER" -lt "$LOCATION_COUNT" ]
do
	LOCATION=$(sed -n "$COUNTER,$COUNTER"'p' /var/lib/samba/netlogon/locations.txt)
	echo '
					<option>'"$LOCATION"'</option>'
	let COUNTER="$COUNTER"+1
done
echo '
				</select>
			</td>
			<td></td>
		</tr>'
echo '
		<tr>
			<td>'$"Department"'</td>
			<td>
				<select class="karoshi-input" tabindex= "3" name="_DEPARTMENT_">
					<option label="blank" value=""></option>
					<option value="'$"Art"'">'$"Art"'</option>
					<option value="'$"Business Studies"'">'$"Business Studies"'</option>
					<option value="'$"Citizenship"'">'$"Citizenship"'</option>
					<option value="'$"Economics"'">'$"Economics"'</option>
					<option value="'$"English"'">'$"English"'</option>
					<option value="'$"Languages"'">'$"Languages"'</option>
					<option value="'$"Geography"'">'$"Geography"'</option>
					<option value="'$"History"'">'$"History"'</option>
					<option value="'$"ICT"'">'$"ICT"'</option>
					<option value="'$"Mathematics"'">'$"Mathematics"'</option>
					<option value="'$"Media Studies"'">'$"Media Studies"'</option>
					<option value="'$"Music"'">'$"Music"'</option>
					<option value="'$"Physical Education"'">'$"Physical Education"'</option>
					<option value="'$"Personal and Social Education"'">'$"Personal and Social Education"'</option>
					<option value="'$"Religious Studies"'">'$"Religious Studies"'</option>
					<option value="'$"Science"'">'$"Science"'</option>
					<option value="'$"Technology"'">'$"Technology"'</option>
					<option value="'$"Office Staff"'">'$"Office Staff"'</option>
					<option value="'$"Other"'">'$"Other"'</option>
					<option label="blank" value="'$""'">'$""'</option>
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>'$"Category"'</td>
			<td>
				<select class="karoshi-input" tabindex= "4" name="_CATEGORY_">
					<option label="blank" value=""></option>
					<option value="'$"Hardware"'">'$"Hardware"'</option>
					<option value="'$"Software"'">'$"Software"'</option>
					<option value="'$"Internet"'">'$"Internet"'</option>
					<option value="'$"Printing"'">'$"Printing"'</option>
					<option value="'$"Whiteboard"'">'$"Whiteboard"'</option>
					<option value="'$"Projector"'">'$"Projector"'</option>
					<option value="'$"Wireless"'">'$"Wireless"'</option>
					<option value="'$"Laptop"'">'$"Laptop"'</option>
					<option value="'$"Online Classroom"'">'$"Online Classroom"'</option>
					<option value="'$"Website"'">'$"Website"'</option>
					<option value="'$"Other"'">'$"Other"'</option>
				</select>
			</td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Choose the category for the problem."'</span></a></td>
		</tr>
		<tr>
			<td>'$"Extended Details"'</td>
			<td><textarea tabindex= "5" cols="70" rows="8" name="_REQUEST_"></textarea></td>
			<td><a class="info icon icon-large fa-info-circle" href="javascript:void(0)"><span class="icon-large-tooltip">'$"Enter in the details for the help request."'</span></a></td>
		</tr>
	</tbody>
</table>
<input value="'$"Submit"'" class="button primary" type="submit"> <input value="'$"Reset"'" class="button" type="reset">
</form>
</display-karoshicontent>
</body>
</html>'
exit
