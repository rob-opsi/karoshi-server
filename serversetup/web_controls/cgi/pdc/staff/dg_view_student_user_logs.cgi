#!/bin/bash
#Copyright (C) 2008  Paul Sharrad
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jsharrad@karoshi.org.uk

#
#Website: http://www.karoshi.org.uk

#Get User Preferences
source /opt/karoshi/web_controls/global_prefs

TITLE=$"Student Internet Logs"
TITLEHELP=$"Internet logs are updated every three minutes."
HELPURL="#"

#Detect mobile browser
source /opt/karoshi/web_controls/detect_mobile_browser

#Generate page layout
source /opt/karoshi/web_controls/generate_page_staff

#Append Karoshi content to the #karoshicontent container.
echo "<display-karoshicontent>
<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

#########################
#Get data input
#########################
DATA=$(cat | tr -cd 'A-Za-z0-9\._:\-')
#########################
#Assign data to variables
#########################
END_POINT=9
SLEEPTIME=5

function get_data {
COUNTER=2
DATAENTRY=""
while [[ $COUNTER -le $END_POINT ]]
do
	DATAHEADER=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
	if [[ "$DATAHEADER" = "$DATANAME" ]]
	then
		let COUNTER="$COUNTER"+1
		DATAENTRY=$(echo "$DATA" | cut -s -d'_' -f"$COUNTER")
		break
	fi
	let COUNTER=$COUNTER+1
done
}


#Assign USERNAME
DATANAME=USERNAME
get_data
USERNAME="$DATAENTRY"

#Assign DATE
DATANAME=DATE
get_data
DATE="$DATAENTRY"

function show_status {
echo '
<script>
	alert("'"$MESSAGE"'");
	window.location = "/cgi-bin/staff/dg_view_student_user_logs_fm.cgi";
</script>
</display-karoshicontent>
</body>
</html>'
exit
}

#########################
#Check data
#########################

#Check to see that USERNAME is not blank
if [ -z "$USERNAME" ]
then
	MESSAGE=$"The username cannot be blank."
	show_status
fi

#Check to see that DATE is not blank
if [ -z "$DATE" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi

DAY=$(echo "$DATE" | cut -d- -f1)
MONTH=$(echo "$DATE" | cut -d- -f2)
YEAR=$(echo "$DATE" | cut -d- -f3)

#Check to see that DAY is not blank
if [ -z "$DAY" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi

#Check to see that MONTH is not blank
if [ -z "$MONTH" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi

#Check to see that YEAR is not blank
if [ -z "$YEAR" ]
then
	MESSAGE=$"The date cannot be blank."
	show_status
fi

#Check that day is not greater than 31
if [[ "$DAY" -gt 31 ]]
then
	MESSAGE=$"Date input error."
	show_status
fi

#Check that the month is not greater than 12
if [[ "$MONTH" -gt 12 ]]
then
	MESSAGE=$"Date input error."
	show_status
fi

if [[ "$YEAR" -lt 2006 ]] || [[ "$YEAR" -gt 3006 ]]
then
	MESSAGE=$"The year is not valid."
	show_status
fi

#Check to see that the user exists
getent passwd "$USERNAME" 1>/dev/null
if [ "$?" != 0 ]
then
	MESSAGE="$USERNAME - "$"This user does not exist."
	show_status
fi

#Check that logs being checked are for a student
STUDENTGROUP=$(id -g -n "$USERNAME")
if [[ $(echo "$STUDENTGROUP" | grep -c ^yr) = 0 ]]
then
	MESSAGE=$"You can only check the logs for a student."
	show_status
fi

#Check to see that the member of staff is not restricted
if [ -f /opt/karoshi/web_controls/staff_restrictions.txt ]
then
	if [[ $(grep -c -w "$REMOTE_USER" /opt/karoshi/web_controls/staff_restrictions.txt) -gt 0 ]]
	then
		sudo -H /opt/karoshi/web_controls/exec/record_staff_error "$REMOTE_USER:$REMOTE_ADDR:$REMOTE_USER"
		sleep "$SLEEPTIME"
		MESSAGE=$"You are not allowed to use this feature."
		show_status
	fi
fi

Checksum=$(sha256sum /var/www/cgi-bin_karoshi/staff/dg_view_student_user_logs.cgi | cut -d' ' -f1)
#View logs
echo "$REMOTE_USER:$REMOTE_ADDR:$Checksum:$USERNAME:$DAY:$MONTH:$YEAR:$REMOTE_USER:$MOBILE" | sudo -H /opt/karoshi/web_controls/exec/dg_view_student_user_logs
EXEC_STATUS="$?"
if [ "$EXEC_STATUS" = 101 ]
then
	MESSAGE=$"There was a problem with this action."
	show_status
fi
if [ "$EXEC_STATUS" = 102 ]
then
	MESSAGE="$USERNAME $DAY-$MONTH-$YEAR : "$"No log for this date."
	show_status
fi

echo '</display-karoshicontent></body></html>'
exit
