#!/bin/bash
#Copyright (C) 2009  Paul Sharrad
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#The Karoshi Team can be contacted at: 
#mpsharrad@karoshi.org.uk
#jharris@karoshi.org.uk
#aball@karoshi.org.uk
#
#Website: http://www.karoshi.org.uk
############################
#Language
############################

STYLESHEET=defaultstyle.css
[ -f /opt/karoshi/web_controls/global_prefs ] && source /opt/karoshi/web_controls/global_prefs
TITLE=$"Web Management"

#Generate head section
source /opt/karoshi/web_controls/generate_head_section

#Generate page top
source /opt/karoshi/web_controls/generate_page_top

#Page content
echo '
<div id="karoshicontent">
</div>
'

#Generate side bar
source /opt/karoshi/web_controls/generate_sidebar_sections

#Add in scripts
/opt/karoshi/web_controls/generate_scripts

#Custom scripts
echo '<script src="/all/js/jquery.tablesorter/jquery.tablesorter.js"></script>
<script id="js">
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
);
</script>
'

#Append Karoshi content to the #karoshicontent container.
echo "
		<display-karoshicontent>
		<script>\$( 'display-karoshicontent' ).appendTo( \"#karoshicontent\" );</script>"

[ -f /var/www/html_karoshi/statistics.html ] && cat /var/www/html_karoshi/statistics.html
echo '
			<br>
			<img src="/images/w3c-html5.png"  height="44" width="100" alt="Valid HTML 5">
		</display-karoshicontent>
	</body>
</html>'
exit
